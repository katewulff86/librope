Licence and Copyright
=====================

**librope** is released under the LGPL-3.0 or later licence, available at
https://spdx.org/licenses/LGPL-3.0-or-later.html.

Copyright 2022 Kate Wulff <katty.wulff@gmail.com>
