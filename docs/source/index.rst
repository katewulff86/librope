.. librope documentation master file, created by
   sphinx-quickstart on Sun Apr 10 21:57:12 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to librope's documentation!
===================================

`librope <https://gitlab.com/katewulff86/librope>`_ is library for the rope
data structure.

What is a rope?
---------------

A rope is a binary tree, with small chunks of text at the leaves. It can perform
many text operations (such as concatenation and splitting) in only
logarithmic time, as opposed to linear time for strings. Ropes are also
more suitable as a purely functional data structure as compared to strings.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   rationale
   building
   usage
   api
   developer
   licence
   references



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
