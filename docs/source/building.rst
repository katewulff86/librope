Building and Installation
=========================

Requirements
------------
- utf8proc (https://juliastrings.github.io/utf8proc/)
- libctest (https://gitlab.com/katewulff86/libctest)

Both of these libraries are included as meson subprojects.

Instructions
------------

**librope** uses the meson build system (https://mesonbuild.com/).

To build, run ::

    $ meson build
    $ ninja -C build

The library can be tested with ::

    $ meson test

And finally installed with ::

    $ meson install

