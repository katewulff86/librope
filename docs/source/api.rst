API Reference
=============

Datatypes
---------
.. doxygengroup:: datatypes
   :content-only:

Constructing ropes
------------------
.. doxygengroup:: rope_constructors
   :content-only:

Basic cursor functions
----------------------
.. doxygengroup:: cursor_functions
   :content-only:

Predicates
----------
.. doxygengroup:: rope_predicates
   :content-only:

Selection
----------
.. doxygengroup:: rope_selection
   :content-only:

Replacement
-----------
.. doxygengroup:: rope_replacement
   :content-only:

Destroying rope and cursor objects
----------------------------------
.. doxygengroup:: memory
   :content-only:
