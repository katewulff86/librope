References
==========

.. [Boehm95]
   Hans J. Boehm, Russ Atkinson and Michale Plass,
   "Ropes: an Alternative to Strings",
   *Software -- Practice and Experience*, vol. 25, no. 12, December 1995.
   Available:
   https://www.cs.tufts.edu/comp/150FP/archive/hans-boehm/ropes.pdf
   [Acccessed 11 Apr., 2022]

.. [SRFI152]
   John Cowan,
   "String Library (reduced)"
   *Scheme Requests for Implementation*
   Available:
   https://srfi.schemers.org/srfi-152/srfi-152.html
   [Acccessed 11 Apr., 2022]

.. [SRFI130]
   John Cowan,
   "Cursor-based string library"
   *Scheme Requests for Implementation*
   Available:
   https://srfi.schemers.org/srfi-130/srfi-130.html
   [Acccessed 11 Apr., 2022]

