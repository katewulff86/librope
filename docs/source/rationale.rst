Rationale
=========

The standard way of representing text in memory is as a *string*, which is a
contiguous block of memory. While this works adequately for short sections
of text, it has some issues when we try to scale up to large amounts of text.

Common text operations, such as concatenation and splitting, require
:math:`O(n)` time and space. Even indexing into a string requires :math:`O(n)`
time when using a variable length encoding such as UTF-8. Strings also do not
work well as a persistent or purely functional data structure, as every
modifying operation requires making a complete copy of the string.

*Ropes* [Boehm95]_ are an alternative data structure for text. They break the text into
small pieces, which are then stored at the leaves of a self-balancing binary
tree. Many common text operations can then be performed in :math:`O(\log n)`
time and space. Ropes are also very suitable as purely functional data
structures, as most of the tree can be reused. For example, changing
a single character requires :math:`O(n)` additional space for an immutable
string, but only :math:`O(\log n)` for a rope.

**librope** implements the rope data structure, and also includes the most
common text processing operations. The ropes are immutable, and so are very
suitable as a text backend for a purely functional language. It also includes
the concept of a *cursor*, which is an abstract pointer into a rope. Using
cursors means we can quickly iterate through each codepoint in a rope.

**librope** was created to be the text backend for a Scheme language
implementation, and so its functions are directly influenced by two SRFIs,
SRFI 152 [SRFI152]_ and SRFI 130 [SRFI130]_. However, it is suitable for
any programming language, or even used directly in a C program.

