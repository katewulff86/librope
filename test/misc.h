#ifndef ROPE_TEST_MISC_H
#define ROPE_TEST_MISC_H

#include <stdlib.h>

#include <rope.h>

#include "fixture.h"

#define assert_equal(x, y) _Generic((x), \
                                    char *: assert_equal_string, \
                                    char const*: assert_equal_string)(x, y, \
                                                                      __FILE__,\
                                                                      __LINE__)

void every_index(rope_fixture_type *, void (*)(rope_fixture_type *, size_t));
void every_index_pair(rope_fixture_type *,
                      void (*)(rope_fixture_type *, size_t, size_t));
void every_cursor_pair(rope_fixture_type *,
                       void (*)(rope_fixture_type *,
                                rope_cursor const *, rope_cursor const *));
void every_ordered_index_pair(rope_fixture_type *,
                              void (*)(rope_fixture_type *, size_t, size_t));
void every_ordered_cursor_pair(rope_fixture_type *,
                               void (*)(rope_fixture_type *,
                                        rope_cursor const *,
                                        rope_cursor const *));
char *substring(char const *, size_t, size_t);
void assert_equal_string(char const *, char const *, char const *, int);

#endif

