#include <stdlib.h>

#include "fixture.h"

static void *
rope_fixture_setup(void *e)
{
    (void) e;
    static int32_t codepoints[] = {
        72, 233, 108, 108, 111, 44, 32, 87, 111, 114, 108, 100, 33,
    };
    rope_fixture_type *object = calloc(sizeof(*object), 1);
    object->empty_string = "";
    object->empty_rope = rope_make_empty();
    object->hello_world_string = "Héllo, World!";
    object->hello_world_codepoints = codepoints;
    object->hello_world_rope = rope_make_string(object->hello_world_string);
    object->long_codepoint = 101;
    object->long_length = 1234567;
    object->long_rope = rope_make_repeating(object->long_length,
                                            object->long_codepoint);
    object->hello_world_length = rope_length(object->hello_world_rope);

    return object;
}

static void
rope_fixture_teardown(void *e)
{
    rope_fixture_type *object = e;
    rope_free(object->hello_world_rope);
    rope_free(object->empty_rope);
    rope_free(object->long_rope);

    assert(rope_get_object_count() == 0);
    free(object);
}

test_fixture rope_fixture = {
    .parent = NULL,
    .setup = rope_fixture_setup,
    .teardown = rope_fixture_teardown,
};

