#ifndef ROPE_TEST_FIXTURE_H
#define ROPE_TEST_FIXTURE_H

#include <stdint.h>
#include <ctest.h>
#include <rope.h>

typedef struct rope_fixture_type {
    char const *empty_string;
    rope *empty_rope;
    char const *hello_world_string;
    int32_t const *hello_world_codepoints;
    rope *hello_world_rope;
    size_t hello_world_length;
    int32_t long_codepoint;
    rope *long_rope;
    size_t long_length;
} rope_fixture_type;

extern test_fixture rope_fixture;

#endif
