#include <string.h>

#include <rope.h>
#include <utf8proc.h>

#include "fixture.h"
#include "test-concatenation.h"

static void
left_empty_append_test(rope_fixture_type *o)
{
    rope *left_append = rope_append(o->empty_rope, o->hello_world_rope);
    char *left_string = rope_extract(left_append);
    assert(strcmp(left_string, o->hello_world_string) == 0);
    free(left_string);
    rope_free(left_append);
}

static void
right_empty_append_test(rope_fixture_type *o)
{
    rope *right_append = rope_append(o->hello_world_rope, o->empty_rope);
    char *right_string = rope_extract(right_append);
    assert(strcmp(right_string, o->hello_world_string) == 0);
    free(right_string);
    rope_free(right_append);
}

static void
empty_empty_append_test(rope_fixture_type *o)
{
    rope *empty_append = rope_append(o->empty_rope, o->empty_rope);
    char *empty_string = rope_extract(empty_append);
    assert(strcmp(empty_string, "") == 0);
    free(empty_string);
    rope_free(empty_append);
}

static void
append_test()
{
    rope *hello_rope = rope_make_string("Héllo, ");
    rope *world_rope = rope_make_string("World!");
    rope *hello_world_rope = rope_append(hello_rope, world_rope);
    rope_free(hello_rope);
    rope_free(world_rope);
    char *extraction = rope_extract(hello_world_rope);
    assert(strcmp(extraction, "Héllo, World!") == 0);
    free(extraction);
    rope_free(hello_world_rope);
}

static void
self_append_test()
{
    rope *hello_rope = rope_make_string("Héllo");
    rope *joined_rope = rope_append(hello_rope, hello_rope);
    rope_free(hello_rope);
    char *extraction = rope_extract(joined_rope);
    assert(strcmp(extraction, "HélloHéllo") == 0);
    free(extraction);
    rope_free(joined_rope);
}

static void
large_append_test()
{
    char *string_a = calloc(sizeof(*string_a), 1001);
    char *string_b = calloc(sizeof(*string_b), 1001);
    for (int i = 0; i < 1000; ++i) {
        string_a[i] = 'a';
        string_b[i] = 'b';
    }
    rope *rope_a = rope_make_string(string_a);
    rope *rope_b = rope_make_string(string_b);
    rope *joined_rope = rope_append(rope_a, rope_b);
    rope_free(rope_a);
    rope_free(rope_b);
    assert(rope_length(joined_rope) == 2000);
    rope_free(joined_rope);
}

static void
many_append_test()
{
    rope *long_rope = rope_make_empty();
    rope *little_rope = rope_make_string("HELLO");
    for (int i = 0; i < 1000; ++i) {
        rope *temp_rope = rope_append(long_rope, little_rope);
        rope_free(long_rope);
        long_rope = temp_rope;
    }
    rope_free(little_rope);
    assert(rope_length(long_rope) == 5000);
    rope_free(long_rope);
}

TEST_CASE_WITH_FIXTURE(left_empty_append_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(right_empty_append_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(empty_empty_append_test, &rope_fixture)
TEST_CASE(append_test)
TEST_CASE(self_append_test)
TEST_CASE(large_append_test)
TEST_CASE(many_append_test)
TEST_SUITE(concatenation_test_suite,
           &left_empty_append_test_case,
           &right_empty_append_test_case,
           &empty_empty_append_test_case,
           &append_test_case,
           &self_append_test_case,
           &large_append_test_case,
           &many_append_test_case)

