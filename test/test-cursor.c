#include <string.h>
#include <rope.h>
#include <utf8proc.h>

#include "fixture.h"
#include "test-cursor.h"

static void
cursor_start_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_start(o->hello_world_rope);
    assert(rope_cursor_ref(cursor) == o->hello_world_codepoints[0]);
    assert(rope_cursor_index(cursor) == 0);
    rope_cursor_free(cursor);
}

static void
cursor_start_empty_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_start(o->empty_rope);
    assert(rope_cursor_ref(cursor) == EOF);
    assert(rope_cursor_index(cursor) == 0);
    rope_cursor_free(cursor);
}

static void
cursor_start_long_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_start(o->long_rope);
    assert(rope_cursor_ref(cursor) == o->long_codepoint);
    assert(rope_cursor_index(cursor) == 0);
    rope_cursor_free(cursor);
}

static void
cursor_end_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_end(o->hello_world_rope);
    assert(rope_cursor_ref(cursor) == EOF);
    assert(rope_cursor_index(cursor) == o->hello_world_length);
    rope_cursor_free(cursor);
}

static void
cursor_end_empty_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_end(o->empty_rope);
    assert(rope_cursor_ref(cursor) == EOF);
    assert(rope_cursor_index(cursor) == 0);
    rope_cursor_free(cursor);
}

static void
cursor_end_long_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_end(o->long_rope);
    assert(rope_cursor_ref(cursor) == EOF);
    assert(rope_cursor_index(cursor) == o->long_length);
    rope_cursor_free(cursor);
}

static void
cursor_next_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_start(o->hello_world_rope);
    for (size_t i = 0; !rope_cursor_end_p(cursor); ++i) {
        assert(rope_cursor_index(cursor) == i);
        assert(rope_cursor_ref(cursor) == o->hello_world_codepoints[i]);
        rope_cursor *cursor_next = rope_cursor_next(cursor);
        rope_cursor_free(cursor);
        cursor = cursor_next;
    }
    rope_cursor_free(cursor);
}

static void
cursor_next_long_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_start(o->long_rope);
    for (size_t i = 0; !rope_cursor_end_p(cursor); ++i) {
        assert(rope_cursor_index(cursor) == i);
        assert(rope_cursor_ref(cursor) == o->long_codepoint);
        rope_cursor *cursor_next = rope_cursor_next(cursor);
        rope_cursor_free(cursor);
        cursor = cursor_next;
    }
    rope_cursor_free(cursor);
}

static void
cursor_next_end_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_end(o->hello_world_rope);
    rope_cursor *cursor_next = rope_cursor_next(cursor);
    assert(rope_cursor_end_p(cursor_next));
    rope_cursor_free(cursor);
    rope_cursor_free(cursor_next);
}

static void
cursor_prev_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_end(o->hello_world_rope);
    for (size_t i = o->hello_world_length-1; !rope_cursor_start_p(cursor);
         --i) {
        rope_cursor *cursor_prev = rope_cursor_prev(cursor);
        rope_cursor_free(cursor);
        cursor = cursor_prev;
        assert(rope_cursor_index(cursor) == i);
        assert(rope_cursor_ref(cursor) == o->hello_world_codepoints[i]);
    }
    rope_cursor_free(cursor);
}

static void
cursor_prev_long_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_end(o->long_rope);
    for (;!rope_cursor_start_p(cursor);) {
        rope_cursor *cursor_prev = rope_cursor_prev(cursor);
        rope_cursor_free(cursor);
        cursor = cursor_prev;
        assert(rope_cursor_ref(cursor) == o->long_codepoint);
    }
    rope_cursor_free(cursor);
}

static void
cursor_prev_start_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_start(o->hello_world_rope);
    rope_cursor *cursor_prev = rope_cursor_prev(cursor);
    assert(rope_cursor_start_p(cursor_prev));
    rope_cursor_free(cursor);
    rope_cursor_free(cursor_prev);
}

static void
cursor_forward_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_start(o->hello_world_rope);
    for (size_t i = 0; i <= o->hello_world_length; ++i) {
        rope_cursor *cursor_forward = rope_cursor_forward(cursor, i);
        assert(rope_cursor_index(cursor_forward) == i);
        if (rope_cursor_index(cursor_forward) == o->hello_world_length)
            assert(rope_cursor_ref(cursor_forward) == EOF);
        else
            assert(rope_cursor_ref(cursor_forward) ==
                   o->hello_world_codepoints[i]);
        rope_cursor_free(cursor_forward);
    }
    rope_cursor_free(cursor);
}

static void
cursor_back_test(rope_fixture_type *o)
{
    rope_cursor *cursor = rope_cursor_make_end(o->hello_world_rope);
    for (size_t i = 0; i <= o->hello_world_length; ++i) {
        rope_cursor *cursor_back = rope_cursor_back(cursor, i);
        assert(rope_cursor_index(cursor_back) == o->hello_world_length - i);
        if (rope_cursor_index(cursor_back) == o->hello_world_length)
            assert(rope_cursor_ref(cursor_back) == EOF);
        else
            assert(rope_cursor_ref(cursor_back) ==
                   o->hello_world_codepoints[o->hello_world_length - i]);
        rope_cursor_free(cursor_back);
    }
    rope_cursor_free(cursor);
}

static void
cursor_eq_test(rope_fixture_type *o)
{
    for (size_t i = 0; i <= o->hello_world_length; ++i) {
        rope_cursor *a = rope_cursor_make(o->hello_world_rope, i);
        for (size_t j = 0; j <= o->hello_world_length; ++j) {
            rope_cursor *b = rope_cursor_make(o->hello_world_rope, j);
            if (i == j) {
                assert(rope_cursor_eq_p(a, b));
                assert(rope_cursor_eq_p(b, a));
            } else {
                assert(!rope_cursor_eq_p(a, b));
                assert(!rope_cursor_eq_p(b, a));
            }
            rope_cursor_free(b);
        }
        rope_cursor_free(a);
    }
}

static void
cursor_lt_test(rope_fixture_type *o)
{
    for (size_t i = 0; i <= o->hello_world_length; ++i) {
        rope_cursor *a = rope_cursor_make(o->hello_world_rope, i);
        for (size_t j = 0; j <= o->hello_world_length; ++j) {
            rope_cursor *b = rope_cursor_make(o->hello_world_rope, j);
            if (i < j)
                assert(rope_cursor_lt_p(a, b));
            else
                assert(!rope_cursor_lt_p(a, b));
            rope_cursor_free(b);
        }
        rope_cursor_free(a);
    }
}

static void
cursor_le_test(rope_fixture_type *o)
{
    for (size_t i = 0; i <= o->hello_world_length; ++i) {
        rope_cursor *a = rope_cursor_make(o->hello_world_rope, i);
        for (size_t j = 0; j <= o->hello_world_length; ++j) {
            rope_cursor *b = rope_cursor_make(o->hello_world_rope, j);
            if (i <= j)
                assert(rope_cursor_le_p(a, b));
            else
                assert(!rope_cursor_le_p(a, b));
            rope_cursor_free(b);
        }
        rope_cursor_free(a);
    }
}

static void
cursor_gt_test(rope_fixture_type *o)
{
    for (size_t i = 0; i <= o->hello_world_length; ++i) {
        rope_cursor *a = rope_cursor_make(o->hello_world_rope, i);
        for (size_t j = 0; j <= o->hello_world_length; ++j) {
            rope_cursor *b = rope_cursor_make(o->hello_world_rope, j);
            if (i > j)
                assert(rope_cursor_gt_p(a, b));
            else
                assert(!rope_cursor_gt_p(a, b));
            rope_cursor_free(b);
        }
        rope_cursor_free(a);
    }
}

static void
cursor_ge_test(rope_fixture_type *o)
{
    for (size_t i = 0; i <= o->hello_world_length; ++i) {
        rope_cursor *a = rope_cursor_make(o->hello_world_rope, i);
        for (size_t j = 0; j <= o->hello_world_length; ++j) {
            rope_cursor *b = rope_cursor_make(o->hello_world_rope, j);
            if (i >= j)
                assert(rope_cursor_ge_p(a, b));
            else
                assert(!rope_cursor_ge_p(a, b));
            rope_cursor_free(b);
        }
        rope_cursor_free(a);
    }
}

static void
cursor_diff_test(rope_fixture_type *o)
{
    for (size_t i = 0; i <= o->hello_world_length; ++i) {
        rope_cursor *a = rope_cursor_make(o->hello_world_rope, i);
        for (size_t j = 0; j <= o->hello_world_length; ++j) {
            rope_cursor *b = rope_cursor_make(o->hello_world_rope, j);
            ptrdiff_t diff = j - i;
            assert(rope_cursor_diff(a, b) == diff);
            assert(rope_cursor_diff(b, a) == -diff);
            rope_cursor_free(b);
        }
        rope_cursor_free(a);
    }
}

static void
cursor_index_test(rope_fixture_type *o)
{
    for (size_t i = 0; i < o->hello_world_length; ++i) {
        rope_cursor *cursor = rope_cursor_make(o->hello_world_rope, i);
        int32_t character = rope_cursor_ref(cursor);
        assert(character == o->hello_world_codepoints[i]);
        rope_cursor_free(cursor);
    }
}

TEST_CASE_WITH_FIXTURE(cursor_start_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_start_empty_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_start_long_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_end_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_end_empty_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_end_long_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_next_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_next_long_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_next_end_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_prev_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_prev_long_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_prev_start_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_forward_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_back_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_eq_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_lt_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_le_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_gt_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_ge_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_diff_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(cursor_index_test, &rope_fixture)
TEST_SUITE(cursor_test_suite,
           &cursor_start_test_case,
           &cursor_start_empty_test_case,
           &cursor_start_long_test_case,
           &cursor_end_test_case,
           &cursor_end_empty_test_case,
           &cursor_end_long_test_case,
           &cursor_next_test_case,
           &cursor_next_long_test_case,
           &cursor_next_end_test_case,
           &cursor_prev_test_case,
           &cursor_prev_long_test_case,
           &cursor_prev_start_test_case,
           &cursor_forward_test_case,
           &cursor_back_test_case,
           &cursor_eq_test_case,
           &cursor_lt_test_case,
           &cursor_le_test_case,
           &cursor_gt_test_case,
           &cursor_ge_test_case,
           &cursor_diff_test_case,
           &cursor_index_test_case)

