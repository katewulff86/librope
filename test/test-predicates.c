#include <string.h>
#include <rope.h>
#include <utf8proc.h>

#include "misc.h"
#include "fixture.h"
#include "test-predicates.h"

static void
null_test(rope_fixture_type *o)
{
    assert(rope_null_p(o->empty_rope));
}

static void
non_null_test(rope_fixture_type *o)
{
    assert(!rope_null_p(o->hello_world_rope));
}

static bool
always_false(int32_t codepoint, void *env)
{
    (void) env;
    (void) codepoint;

    return false;
}

static bool
always_true(int32_t codepoint, void *env)
{
    (void) env;
    (void) codepoint;

    return true;
}

static bool
only_l(int32_t codepoint, void *env)
{
    (void) env;

    return codepoint == 108;
}

static void
any_false_test(rope_fixture_type *o)
{
    assert(!rope_any(o->hello_world_rope, always_false, NULL));
}

static void
any_true_test(rope_fixture_type *o)
{
    assert(rope_any(o->hello_world_rope, always_true, NULL));
}

static void
any_l_test(rope_fixture_type *o)
{
    assert(rope_any(o->hello_world_rope, only_l, NULL));
}

static void
any_empty_test(rope_fixture_type *o)
{
    assert(!rope_any(o->empty_rope, always_false, NULL));
    assert(!rope_any(o->empty_rope, always_true, NULL));
    assert(!rope_any(o->empty_rope, only_l, NULL));
}

static void
any_false_start_end_test_aux(rope_fixture_type *o, size_t start, size_t end)
{
    assert(!rope_any_start_end(o->hello_world_rope, always_false, NULL,
                               start, end));
}

static void
any_false_start_end_test(rope_fixture_type *o)
{
    every_ordered_index_pair(o, any_false_start_end_test_aux);
}

static void
any_true_start_end_test_aux(rope_fixture_type *o, size_t start, size_t end)
{
    if (start == end)
        assert(!rope_any_start_end(o->hello_world_rope, always_true, NULL,
                                   start, end));
    else
        assert(rope_any_start_end(o->hello_world_rope, always_true, NULL,
                                  start, end));
}

static void
any_true_start_end_test(rope_fixture_type *o)
{
    every_ordered_index_pair(o, any_true_start_end_test_aux);
}

static void
any_false_cursor_test_aux(rope_fixture_type *o, rope_cursor const *start_cursor,
                          rope_cursor const *end_cursor)
{
    (void) o;
    assert(!rope_any_cursor(always_false, NULL, start_cursor, end_cursor));
}

static void
any_false_cursor_test(rope_fixture_type *o)
{
    every_ordered_cursor_pair(o, any_false_cursor_test_aux);
}

static void
any_true_cursor_test_aux(rope_fixture_type *o,
                         rope_cursor const *start_cursor,
                          rope_cursor const *end_cursor)
{
    (void) o;
    if (rope_cursor_lt_p(start_cursor, end_cursor))
        assert(rope_any_cursor(always_true, NULL, start_cursor, end_cursor));
    else
        assert(!rope_any_cursor(always_true, NULL, start_cursor, end_cursor));
}

static void
any_true_cursor_test(rope_fixture_type *o)
{
    every_ordered_cursor_pair(o, any_true_cursor_test_aux);
}

static void
every_false_test(rope_fixture_type *o)
{
    assert(!rope_every(o->hello_world_rope, always_false, NULL));
}

static void
every_true_test(rope_fixture_type *o)
{
    assert(rope_every(o->hello_world_rope, always_true, NULL));
}

static void
every_l_test(rope_fixture_type *o)
{
    assert(!rope_every(o->hello_world_rope, only_l, NULL));
}

static void
every_empty_test(rope_fixture_type *o)
{
    assert(rope_every(o->empty_rope, always_false, NULL));
    assert(rope_every(o->empty_rope, always_true, NULL));
    assert(rope_every(o->empty_rope, only_l, NULL));
}

static void
every_false_start_end_test_aux(rope_fixture_type *o, size_t start, size_t end)
{
    if (start < end)
        assert(!rope_every_start_end(o->hello_world_rope, always_false,
                                     NULL, start, end));
    else
        assert(rope_every_start_end(o->hello_world_rope, always_false,
                                    NULL, start, end));
}

static void
every_false_start_end_test(rope_fixture_type *o)
{
    every_ordered_index_pair(o, every_false_start_end_test_aux);
}

static void
every_true_start_end_test_aux(rope_fixture_type *o, size_t start, size_t end)
{
    assert(rope_every_start_end(o->hello_world_rope, always_true,
                                NULL, start, end));
}

static void
every_true_start_end_test(rope_fixture_type *o)
{
    every_ordered_index_pair(o, every_true_start_end_test_aux);
}

static void
every_false_cursor_test_aux(rope_fixture_type *o,
                            rope_cursor const *start_cursor,
                            rope_cursor const *end_cursor)
{
    (void) o;
    if (rope_cursor_lt_p(start_cursor, end_cursor))
        assert(!rope_every_cursor(always_false, NULL,
                                  start_cursor, end_cursor));
    else
        assert(rope_every_cursor(always_false, NULL,
                                 start_cursor, end_cursor));
}

static void
every_false_cursor_test(rope_fixture_type *o)
{
    every_ordered_cursor_pair(o, every_false_cursor_test_aux);
}

static void
every_true_cursor_test_aux(rope_fixture_type *o,
                           rope_cursor const *start_cursor,
                           rope_cursor const *end_cursor)
{
    (void) o;
    assert(rope_every_cursor(always_true, NULL, start_cursor, end_cursor));
}

static void
every_true_cursor_test(rope_fixture_type *o)
{
    every_ordered_cursor_pair(o, every_true_cursor_test_aux);
}

TEST_CASE_WITH_FIXTURE(null_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(non_null_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(any_false_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(any_true_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(any_l_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(any_empty_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(any_false_start_end_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(any_true_start_end_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(any_false_cursor_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(any_true_cursor_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(every_false_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(every_true_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(every_l_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(every_empty_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(every_false_start_end_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(every_true_start_end_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(every_false_cursor_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(every_true_cursor_test, &rope_fixture)
TEST_SUITE(predicates_test_suite,
           &null_test_case,
           &non_null_test_case,
           &any_false_test_case,
           &any_true_test_case,
           &any_l_test_case,
           &any_empty_test_case,
           &any_false_start_end_test_case,
           &any_true_start_end_test_case,
           &any_false_cursor_test_case,
           &any_true_cursor_test_case,
           &every_false_test_case,
           &every_true_test_case,
           &every_l_test_case,
           &every_empty_test_case,
           &every_false_start_end_test_case,
           &every_true_start_end_test_case,
           &every_false_cursor_test_case,
           &every_true_cursor_test_case)

