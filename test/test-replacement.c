#include <string.h>
#include <rope.h>
#include <utf8proc.h>

#include "misc.h"
#include "fixture.h"
#include "test-replacement.h"

typedef struct replacement_fixture_type {
    char *first_string;
    char *second_string;
    rope *first_rope;
    rope *second_rope;
    size_t start;
    size_t end;
    char *result_string;
    rope *result_rope;
} replacement_fixture_type;

static replacement_fixture_type *
replacement_fixture_setup(replacement_fixture_type *o)
{
    o = calloc(sizeof(*o), 1);
    o->first_string = "this is my lovely test string";
    o->second_string = "so amazing";
    o->first_rope = rope_make_string(o->first_string);
    o->second_rope = rope_make_string(o->second_string);
    o->start = 11;
    o->end = 17;
    o->result_string = "this is my so amazing test string";
    o->result_rope = rope_make_string(o->result_string);

    return o;
}

static void
replacement_fixture_teardown(replacement_fixture_type *o)
{
    rope_free(o->first_rope);
    rope_free(o->second_rope);
    rope_free(o->result_rope);
    free(o);
}

test_fixture replacement_fixture = {
    .parent = NULL,
    .setup = (test_setup_func) replacement_fixture_setup,
    .teardown = (test_teardown_func) replacement_fixture_teardown,
};

static void
replace_test(replacement_fixture_type *o)
{
    rope *replaced = rope_replace(o->first_rope, o->second_rope,
                                  o->start, o->end);
    assert(rope_eq_p(replaced, o->result_rope));
    rope_free(replaced);
}

TEST_CASE_WITH_FIXTURE(replace_test, &replacement_fixture)
TEST_SUITE(replacement_test_suite,
           &replace_test_case)

