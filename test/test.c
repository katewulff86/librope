#include <string.h>
#include <ctest.h>
#include <utf8proc.h>
#include <rope.h>

#include "test-concatenation.h"
#include "test-constructors.h"
#include "test-cursor.h"
#include "test-predicates.h"
#include "test-replacement.h"
#include "test-selection.h"
#include "test-splitting.h"

TEST_SUITE(librope_test_suite,
           &concatenation_test_suite,
           &constructors_test_suite,
           &cursor_test_suite,
           &predicates_test_suite,
           &replacement_test_suite,
           &selection_test_suite,
           &splitting_test_suite)

TEST_MAIN(&librope_test_suite)

