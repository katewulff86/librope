#include <string.h>
#include <rope.h>
#include <utf8proc.h>

#include "fixture.h"
#include "test-constructors.h"

static void
empty_length_test(rope_fixture_type *o)
{
    assert(rope_length(o->empty_rope) == 0);
}

static void
hello_world_length_test(rope_fixture_type *o)
{
    assert(rope_length(o->hello_world_rope) == o->hello_world_length);
}

static void
empty_repeating_test()
{
    rope *repeating_rope = rope_make_repeating(0, 100);
    assert(rope_length(repeating_rope) == 0);
    rope_free(repeating_rope);
}

static void
invalid_repeating_test()
{
    rope *repeating_rope = rope_make_repeating(1000, -1);
    assert(repeating_rope == NULL);
}

static void
repeating_length_test()
{
    size_t const LONG_LENGTH = 1234567;
    int32_t codepoint;
    utf8proc_iterate((uint8_t *) "é", -1, &codepoint);
    rope *repeating_rope = rope_make_repeating(LONG_LENGTH, codepoint);
    assert(rope_length(repeating_rope) == LONG_LENGTH);
    rope_free(repeating_rope);

    assert(rope_get_object_count() == 0);
}

static int32_t
tabulation_lambda(size_t i, void *e)
{
    rope_fixture_type *o = e;

    return o->hello_world_codepoints[i];
}

static void
tabulation_test(rope_fixture_type *o)
{
    rope *r = rope_make_tabulate(tabulation_lambda, o, 13);
    char const *extract = rope_extract(r);
    assert(strcmp(extract, o->hello_world_string) == 0);
    rope_free(r);
}

static int32_t
invalid_tabulation_lambda(size_t i, void *e)
{
    (void) i;
    (void) e;

    return -1;
}

static void
invalid_tabulation_test()
{
    rope *r = rope_make_tabulate(invalid_tabulation_lambda, NULL, 13);
    assert(rope_length(r) == 0);
    rope_free(r);
}

static int32_t
generator(void *e)
{
    static int32_t hello_world[] = {
        72, 101, 108, 108, 111, 44, 32, 87, 111, 114, 108, 100, 33, EOF,
    };
    size_t *index = e;

    int32_t character = hello_world[*index];
    *index += 1;

    return character;
}

static void
generator_test()
{
    size_t index = 0;
    rope *r = rope_make_generator(generator, &index);
    char const *extract = rope_extract(r);
    assert(strcmp(extract, "Hello, World!") == 0);
    rope_free(r);
}

static void
generator_right_test()
{
    size_t index = 0;
    rope *r = rope_make_generator_right(generator, &index);
    char const *extract = rope_extract(r);
    assert(strcmp(extract, "!dlroW ,olleH") == 0);
    rope_free(r);
}

static void
clone_test(rope_fixture_type *o)
{
    rope *clone = rope_clone(o->hello_world_rope);
    assert(rope_length(clone) == rope_length(o->hello_world_rope));
    rope_free(clone);
}

TEST_CASE_WITH_FIXTURE(empty_length_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(hello_world_length_test, &rope_fixture)
TEST_CASE(empty_repeating_test)
TEST_CASE(invalid_repeating_test)
TEST_CASE(repeating_length_test)
TEST_CASE_WITH_FIXTURE(tabulation_test, &rope_fixture)
TEST_CASE(invalid_tabulation_test)
TEST_CASE(generator_test)
TEST_CASE(generator_right_test)
TEST_CASE_WITH_FIXTURE(clone_test, &rope_fixture)
TEST_SUITE(constructors_test_suite,
           &empty_length_test_case,
           &hello_world_length_test_case,
           &empty_repeating_test_case,
           &invalid_repeating_test_case,
           &repeating_length_test_case,
           &tabulation_test_case,
           &invalid_tabulation_test_case,
           &generator_test_case,
           &generator_right_test_case,
           &clone_test_case)

