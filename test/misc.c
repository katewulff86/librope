#include <string.h>

#include <utf8proc.h>

#include "misc.h"

static size_t
codepoint_offset(uint8_t const *data, size_t index)
{
    size_t offset = 0;
    for (; index > 0; --index) {
        size_t bytes = utf8proc_utf8class[data[offset]];
        offset += bytes;
    }

    return offset;
}

void
every_index(rope_fixture_type *o, void (*func)(rope_fixture_type *, size_t))
{
    for (size_t i = 0; i < o->hello_world_length; ++i)
        func(o, i);
}

void
every_index_pair(rope_fixture_type *o,
                 void (*func)(rope_fixture_type *, size_t, size_t))
{
    for (size_t start = 0; start < o->hello_world_length; ++start) {
        for (size_t end = 0; end < o->hello_world_length; ++end) {
            func(o, start, end);
        }
    }
}

void
every_cursor_pair(rope_fixture_type *o,
                  void (*func)(rope_fixture_type *,
                               rope_cursor const *, rope_cursor const *))
{
    for (size_t start = 0; start < o->hello_world_length; ++start) {
        rope_cursor *start_cursor = rope_cursor_make(o->hello_world_rope,
                                                     start);
        for (size_t end = 0; end < o->hello_world_length; ++end) {
            rope_cursor *end_cursor = rope_cursor_make(o->hello_world_rope,
                                                       end);
            func(o, start_cursor, end_cursor);
            rope_cursor_free(end_cursor);
        }
        rope_cursor_free(start_cursor);
    }
}

void
every_ordered_index_pair(rope_fixture_type *o,
                 void (*func)(rope_fixture_type *, size_t, size_t))
{
    for (size_t start = 0; start < o->hello_world_length; ++start) {
        for (size_t end = start; end < o->hello_world_length; ++end) {
            func(o, start, end);
        }
    }
}

void
every_ordered_cursor_pair(rope_fixture_type *o,
                  void (*func)(rope_fixture_type *,
                               rope_cursor const *, rope_cursor const *))
{
    for (size_t start = 0; start < o->hello_world_length; ++start) {
        rope_cursor *start_cursor = rope_cursor_make(o->hello_world_rope,
                                                     start);
        for (size_t end = start; end < o->hello_world_length; ++end) {
            rope_cursor *end_cursor = rope_cursor_make(o->hello_world_rope,
                                                       end);
            func(o, start_cursor, end_cursor);
            rope_cursor_free(end_cursor);
        }
        rope_cursor_free(start_cursor);
    }
}

char *
substring(char const *string, size_t start, size_t end)
{
    size_t substring_length = end - start;
    size_t substring_size = substring_length * 4;
    char *substring = calloc(sizeof(*substring), substring_size + 1);
    size_t start_offset = codepoint_offset((uint8_t *) string, start);
    size_t end_offset = codepoint_offset((uint8_t *) string, end);
    size_t size = end_offset - start_offset;
    memcpy(substring, &string[start_offset], size);

    return substring;
}

void
assert_equal_string(char const *actual, char const *expected,
                    char const *file, int line_number)
{
    if (strcmp(actual, expected)) {
        fprintf(stderr, "%s:%i: assert equal failed\n"
                "expected: \"%s\", actual: \"%s\"\n", file, line_number,
                actual, expected);
        abort();
    }
}

