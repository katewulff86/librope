#include <stdlib.h>
#include <string.h>
#include <rope.h>
#include <utf8proc.h>

#include "misc.h"
#include "fixture.h"
#include "test-selection.h"

static void
extract_test(rope_fixture_type *o)
{
    char *extraction = rope_extract(o->hello_world_rope);
    assert_equal(o->hello_world_string, extraction);
    free(extraction);
}

static void
extract_empty_test(rope_fixture_type *o)
{
    char *extraction = rope_extract(o->empty_rope);
    assert_equal("", extraction);
    free(extraction);
}

static void
extract_start_end_test_aux(rope_fixture_type *o, size_t start, size_t end)
{
    char *extraction = rope_extract_start_end(o->hello_world_rope, start, end);

    if (end == start) {
        assert_equal("", extraction);
    } else {
        char *hello_substring = substring(o->hello_world_string, start, end);
        assert_equal(hello_substring, extraction);
        free(hello_substring);
    }

    free(extraction);
}

static void
extract_start_end_test(rope_fixture_type *o)
{
    every_ordered_index_pair(o, extract_start_end_test_aux);
}

static void
extract_cursor_test_aux(rope_fixture_type *o,
                        rope_cursor const *start_cursor,
                        rope_cursor const *end_cursor)
{
    char *extraction = rope_extract_cursor(start_cursor, end_cursor);

    if (rope_cursor_eq_p(end_cursor, start_cursor)) {
        assert(strcmp(extraction, "") == 0);
    } else {
        char *hello_substring = substring(o->hello_world_string,
                                          rope_cursor_index(start_cursor),
                                          rope_cursor_index(end_cursor));
        assert_equal(hello_substring, extraction);
        free(hello_substring);
    }

    free(extraction);
}

static void
extract_cursor_test(rope_fixture_type *o)
{
    every_ordered_cursor_pair(o, extract_cursor_test_aux);
}

static void
rope_length_test(rope_fixture_type *o)
{
    assert(rope_length(o->hello_world_rope) == o->hello_world_length);
}

static void
rope_ref_test(rope_fixture_type *o)
{
    for (size_t i = 0; i < o->hello_world_length; ++i)
        assert(rope_ref(o->hello_world_rope, i) ==
               o->hello_world_codepoints[i]);
    assert(rope_ref(o->hello_world_rope, o->hello_world_length) == EOF);
}

static void
subrope_start_end_test_aux(rope_fixture_type *o, size_t start, size_t end)
{
    rope *subrope = rope_subrope_start_end(o->hello_world_rope, start, end);

    if (end == start) {
        assert(rope_null_p(subrope));
    } else {
        char *extraction = rope_extract(subrope);
        char *hello_substring = substring(o->hello_world_string, start, end);
        assert_equal(hello_substring, extraction);
        free(hello_substring);
        free(extraction);
    }

    rope_free(subrope);
}

static void
subrope_start_end_test(rope_fixture_type *o)
{
    every_ordered_index_pair(o, subrope_start_end_test_aux);
}

static void
subrope_cursor_test_aux(rope_fixture_type *o,
                        rope_cursor const *start_cursor,
                        rope_cursor const *end_cursor)
{
    rope *subrope = rope_subrope_cursor(start_cursor, end_cursor);

    if (rope_cursor_eq_p(end_cursor, start_cursor)) {
        assert(rope_null_p(subrope));
    } else {
        char *extraction = rope_extract(subrope);
        char *hello_substring = substring(o->hello_world_string,
                                          rope_cursor_index(start_cursor),
                                          rope_cursor_index(end_cursor));
        assert_equal(hello_substring, extraction);
        free(hello_substring);
        free(extraction);
    }

    rope_free(subrope);
}

static void
subrope_cursor_test(rope_fixture_type *o)
{
    every_ordered_cursor_pair(o, subrope_cursor_test_aux);
}

static void
take_test_aux(rope_fixture_type *o, size_t i)
{
    rope *taken = rope_take(o->hello_world_rope, i);
    char *extraction = rope_extract(taken);
    char *string = substring(o->hello_world_string, 0, i);
    assert_equal(string, extraction);
    free(string);
    free(extraction);
    rope_free(taken);
}

static void
take_test(rope_fixture_type *o)
{
    every_index(o, take_test_aux);
}

static void
drop_test_aux(rope_fixture_type *o, size_t i)
{
    rope *taken = rope_drop(o->hello_world_rope, i);
    char *extraction = rope_extract(taken);
    char *string = substring(o->hello_world_string, i, o->hello_world_length);
    assert_equal(string, extraction);
    free(string);
    free(extraction);
    rope_free(taken);
}

static void
drop_test(rope_fixture_type *o)
{
    every_index(o, drop_test_aux);
}

static void
take_right_test_aux(rope_fixture_type *o, size_t i)
{
    rope *taken = rope_take_right(o->hello_world_rope, i);
    char *extraction = rope_extract(taken);
    char *string = substring(o->hello_world_string,
                             o->hello_world_length - i,
                             o->hello_world_length);
    assert_equal(string, extraction);
    free(string);
    free(extraction);
    rope_free(taken);
}

static void
take_right_test(rope_fixture_type *o)
{
    every_index(o, take_right_test_aux);
}

static void
drop_right_test_aux(rope_fixture_type *o, size_t i)
{
    rope *taken = rope_drop_right(o->hello_world_rope, i);
    char *extraction = rope_extract(taken);
    char *string = substring(o->hello_world_string, 0,
                             o->hello_world_length - i);
    assert_equal(string, extraction);
    free(string);
    free(extraction);
    rope_free(taken);
}

static void
drop_right_test(rope_fixture_type *o)
{
    every_index(o, drop_right_test_aux);
}

typedef struct pad_fixture_type {
    rope_fixture_type f;
    size_t shorten_amount;
    size_t lengthen_amount;
    size_t short_length;
    size_t sub_short_length;
    size_t length;
    size_t sub_length;
    size_t long_length;
    size_t sub_long_length;
    int32_t codepoint;
    size_t start;
    size_t end;
    rope_cursor *start_cursor;
    rope_cursor *end_cursor;
    rope *subrope;
    char *subrope_string;
} pad_fixture_type;

static pad_fixture_type *
rope_fixture_setup(pad_fixture_type *o)
{
    o = realloc(o, sizeof(*o));
    o->shorten_amount = 5;
    o->lengthen_amount = 7;
    o->short_length = o->f.hello_world_length - o->shorten_amount;
    o->sub_short_length = o->short_length - 2;
    o->length = o->f.hello_world_length;
    o->sub_length = o->length - 2;
    o->long_length = o->f.hello_world_length + o->lengthen_amount;
    o->sub_long_length = o->long_length - 2;
    o->codepoint = 32;
    o->start = 1;
    o->end = o->f.hello_world_length - 1;
    o->start_cursor = rope_cursor_make(o->f.hello_world_rope, o->start);
    o->end_cursor = rope_cursor_make(o->f.hello_world_rope, o->end);
    o->subrope = rope_subrope_cursor(o->start_cursor, o->end_cursor);
    o->subrope_string = rope_extract(o->subrope);

    return o;
}

static void
rope_fixture_teardown(pad_fixture_type *o)
{
    rope_cursor_free(o->start_cursor);
    rope_cursor_free(o->end_cursor);
    rope_free(o->subrope);
    free(o->subrope_string);
}

test_fixture pad_fixture = {
    .parent = &rope_fixture,
    .setup = (test_setup_func) rope_fixture_setup,
    .teardown = (test_teardown_func) rope_fixture_teardown,
};

static void
pad_short_test(pad_fixture_type *o)
{
    rope *pad = rope_pad(o->f.hello_world_rope, o->short_length, o->codepoint);
    char *extraction = rope_extract(pad);
    char *string = substring(o->f.hello_world_string, o->shorten_amount,
                             o->f.hello_world_length);
    assert_equal(string, extraction);
    free(string); free(extraction);
    rope_free(pad);
}

static void
pad_exact_test(pad_fixture_type *o)
{
    rope *pad = rope_pad(o->f.hello_world_rope, o->length, o->codepoint);
    assert(rope_eq_p(pad, o->f.hello_world_rope));
    rope_free(pad);
}

static void
pad_long_test(pad_fixture_type *o)
{
    rope *pad = rope_pad(o->f.hello_world_rope, o->long_length, o->codepoint);
    char *extraction = rope_extract(pad);
    char *prefix = substring(extraction, 0, o->lengthen_amount);
    char *suffix = substring(extraction, o->lengthen_amount,  o->long_length);
    for (size_t i = 0; i < o->lengthen_amount; ++i)
        assert(rope_ref(pad, i) == o->codepoint);
    assert_equal(o->f.hello_world_string, suffix);
    free(prefix); free(suffix); free(extraction);
    rope_free(pad);
}

static void
pad_start_end_short_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_start_end(o->f.hello_world_rope, o->sub_short_length,
                                   o->codepoint, o->start, o->end);
    char *extraction = rope_extract(pad);
    char *string = substring(o->f.hello_world_string,
                             o->shorten_amount + o->start, o->end);
    assert_equal(string, extraction);
    free(string); free(extraction);
    rope_free(pad);
}

static void
pad_start_end_exact_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_start_end(o->f.hello_world_rope, o->sub_length,
                                   o->codepoint, o->start, o->end);
    assert(rope_eq_p(o->subrope, pad));
    rope_free(pad);
}

static void
pad_start_end_long_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_start_end(o->f.hello_world_rope, o->sub_long_length,
                                   o->codepoint, o->start, o->end);
    char *extraction = rope_extract(pad);
    char *prefix = substring(extraction, 0, o->lengthen_amount);
    char *suffix = substring(extraction, o->lengthen_amount,
                             o->sub_long_length);
    for (size_t i = 0; i < o->lengthen_amount; ++i)
        assert(rope_ref(pad, i) == o->codepoint);
    assert_equal(o->subrope_string, suffix);
    free(prefix); free(suffix); free(extraction);
    rope_free(pad);
}

static void
pad_cursor_short_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_cursor(o->sub_short_length, o->codepoint,
                                o->start_cursor, o->end_cursor);
    char *extraction = rope_extract(pad);
    char *string = substring(o->f.hello_world_string,
                             o->shorten_amount + o->start, o->end);
    assert_equal(string, extraction);
    free(string); free(extraction);
    rope_free(pad);
}

static void
pad_cursor_exact_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_cursor(o->sub_length, o->codepoint,
                                o->start_cursor, o->end_cursor);
    assert(rope_eq_p(o->subrope, pad));
    rope_free(pad);
}

static void
pad_cursor_long_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_cursor(o->sub_long_length, o->codepoint,
                                o->start_cursor, o->end_cursor);
    char *extraction = rope_extract(pad);
    char *prefix = substring(extraction, 0, o->lengthen_amount);
    char *suffix = substring(extraction, o->lengthen_amount,
                             o->sub_long_length);
    for (size_t i = 0; i < o->lengthen_amount; ++i)
        assert(rope_ref(pad, i) == o->codepoint);
    assert_equal(o->subrope_string, suffix);
    free(prefix); free(suffix); free(extraction);
    rope_free(pad);
}

static void
pad_right_short_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_right(o->f.hello_world_rope, o->short_length,
                               o->codepoint);
    char *extraction = rope_extract(pad);
    char *string = substring(o->f.hello_world_string, 0,
                             o->f.hello_world_length - o->shorten_amount);
    assert_equal(string, extraction);
    free(string); free(extraction);
    rope_free(pad);
}

static void
pad_right_exact_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_right(o->f.hello_world_rope, o->length, o->codepoint);
    assert(rope_eq_p(pad, o->f.hello_world_rope));
    rope_free(pad);
}

static void
pad_right_long_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_right(o->f.hello_world_rope, o->long_length,
                               o->codepoint);
    char *extraction = rope_extract(pad);
    char *prefix = substring(extraction, 0, o->length);
    char *suffix = substring(extraction, o->length, o->long_length);
    assert_equal(o->f.hello_world_string, prefix);
    for (size_t i = o->length; i < o->long_length; ++i)
        assert(rope_ref(pad, i) == o->codepoint);
    free(prefix); free(suffix); free(extraction);
    rope_free(pad);
}

static void
pad_right_start_end_short_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_right_start_end(o->f.hello_world_rope,
                                         o->sub_short_length, o->codepoint,
                                         o->start, o->end);
    char *extraction = rope_extract(pad);
    char *string = substring(o->f.hello_world_string,
                             o->start, o->sub_short_length + o->start);
    assert_equal(string, extraction);
    free(string); free(extraction);
    rope_free(pad);
}

static void
pad_right_start_end_exact_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_right_start_end(o->f.hello_world_rope, o->sub_length,
                                         o->codepoint, o->start, o->end);
    assert(rope_eq_p(o->subrope, pad));
    rope_free(pad);
}

static void
pad_right_start_end_long_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_right_start_end(o->f.hello_world_rope,
                                         o->sub_long_length, o->codepoint,
                                         o->start, o->end);
    char *extraction = rope_extract(pad);
    char *prefix = substring(extraction, 0, o->sub_length);
    char *suffix = substring(extraction, o->sub_length,
                             o->sub_long_length);
    assert_equal(o->subrope_string, prefix);
    for (size_t i = o->sub_length; i < o->sub_long_length; ++i)
        assert(rope_ref(pad, i) == o->codepoint);
    free(prefix); free(suffix); free(extraction);
    rope_free(pad);
}

static void
pad_right_cursor_short_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_right_cursor(o->sub_short_length, o->codepoint,
                                      o->start_cursor, o->end_cursor);
    char *extraction = rope_extract(pad);
    char *string = substring(o->f.hello_world_string,
                             o->start, o->sub_short_length + o->start);
    assert_equal(string, extraction);
    free(string); free(extraction);
    rope_free(pad);
}

static void
pad_right_cursor_exact_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_right_cursor(o->sub_length, o->codepoint,
                                      o->start_cursor, o->end_cursor);
    assert(rope_eq_p(o->subrope, pad));
    rope_free(pad);
}

static void
pad_right_cursor_long_test(pad_fixture_type *o)
{
    rope *pad = rope_pad_right_cursor(o->sub_long_length, o->codepoint,
                                      o->start_cursor, o->end_cursor);
    char *extraction = rope_extract(pad);
    char *prefix = substring(extraction, 0, o->sub_length);
    char *suffix = substring(extraction, o->sub_length,
                             o->sub_long_length);
    assert_equal(o->subrope_string, prefix);
    for (size_t i = o->sub_length; i < o->sub_long_length; ++i)
        assert(rope_ref(pad, i) == o->codepoint);
    free(prefix); free(suffix); free(extraction);
    rope_free(pad);
}

TEST_CASE_WITH_FIXTURE(extract_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(extract_empty_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(extract_start_end_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(extract_cursor_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(rope_length_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(rope_ref_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(subrope_start_end_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(subrope_cursor_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(take_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(drop_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(take_right_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(drop_right_test, &rope_fixture)
TEST_CASE_WITH_FIXTURE(pad_short_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_exact_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_long_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_start_end_short_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_start_end_exact_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_start_end_long_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_cursor_short_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_cursor_exact_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_cursor_long_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_right_short_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_right_exact_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_right_long_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_right_start_end_short_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_right_start_end_exact_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_right_start_end_long_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_right_cursor_short_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_right_cursor_exact_test, &pad_fixture)
TEST_CASE_WITH_FIXTURE(pad_right_cursor_long_test, &pad_fixture)
TEST_SUITE(selection_test_suite,
           &extract_test_case,
           &extract_empty_test_case,
           &extract_start_end_test_case,
           &extract_cursor_test_case,
           &rope_length_test_case,
           &rope_ref_test_case,
           &subrope_start_end_test_case,
           &subrope_cursor_test_case,
           &take_test_case,
           &drop_test_case,
           &take_right_test_case,
           &drop_right_test_case,
           &pad_short_test_case,
           &pad_exact_test_case,
           &pad_long_test_case,
           &pad_start_end_short_test_case,
           &pad_start_end_exact_test_case,
           &pad_start_end_long_test_case,
           &pad_cursor_short_test_case,
           &pad_cursor_exact_test_case,
           &pad_cursor_long_test_case,
           &pad_right_short_test_case,
           &pad_right_exact_test_case,
           &pad_right_long_test_case,
           &pad_right_start_end_short_test_case,
           &pad_right_start_end_exact_test_case,
           &pad_right_start_end_long_test_case,
           &pad_right_cursor_short_test_case,
           &pad_right_cursor_exact_test_case,
           &pad_right_cursor_long_test_case)

