#include <string.h>
#include <rope.h>
#include <utf8proc.h>

#include "misc.h"
#include "fixture.h"
#include "test-splitting.h"

static void
bifurcation_test(rope_fixture_type *o)
{
    for (size_t i = 0; i < o->hello_world_length; ++i) {
        rope *prefix, *suffix;
        rope_bifurcate(o->hello_world_rope, i, &prefix, &suffix);
        char *prefix_extraction = rope_extract(prefix);
        char *suffix_extraction = rope_extract(suffix);
        char *prefix_string = substring(o->hello_world_string, 0, i);
        char *suffix_string = substring(o->hello_world_string, i,
                                        o->hello_world_length);

        assert(strcmp(prefix_extraction, prefix_string) == 0);
        assert(strcmp(suffix_extraction, suffix_string) == 0);

        rope_free(prefix); rope_free(suffix);
        free(prefix_extraction); free(suffix_extraction);
        free(prefix_string); free(suffix_string);
    }
}

TEST_CASE_WITH_FIXTURE(bifurcation_test, &rope_fixture)
TEST_SUITE(splitting_test_suite,
           &bifurcation_test_case)

