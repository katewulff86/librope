/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>

#include "cursor.h"
#include "memory.h"

rope_cursor *
rope_cursor_make_raw(rope_path *path, rope_node *root,
                     size_t index, size_t offset)
{
    rope_cursor *cursor = rope_allocate(sizeof(*cursor));
    cursor->path = rope_path_rc_keep(path);
    cursor->root = rope_node_rc_keep(root);
    cursor->index = index;
    cursor->offset = offset;

    return cursor;
}

/**
 * \brief Destroy a cursor
 * \ingroup memory
 *
 * Destroy a cursor and free its associated memory. Any data reachable from only
 * this cursor will also be freed.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] cursor cursor to be destroyed
 */
void
rope_cursor_free(rope_cursor *cursor)
{
    rope_path_rc_drop(cursor->path);
    rope_node_rc_drop(cursor->root);
    rope_deallocate(cursor);
}

/**
 * \brief Calculate the difference between two cursors
 * \ingroup cursor_functions
 *
 * Return the difference between two cursors. That is, the number of codepoints
 * that separate the two. This means that
 * ```c
 * rope_cursor_diff(rope_cursor_make_start(my_rope),
 *                  rope_cursor_make_end(my_rope);
 * ```
 * will return the length of the rope. The return value is unspecified if the
 * two cursors do not point to the same rope.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] a first cursor into a rope
 * \param[in] b second cursor into a rope
 * \return the number of codepoints separating the two
 */
ptrdiff_t
rope_cursor_diff(rope_cursor const *a, rope_cursor const *b)
{
    return b->index - a->index;
}

/**
 * \brief Get the index of a cursor
 * \ingroup cursor_functions
 *
 * Returns how far into a rope a given cursor points. For example,
 * ```c
 * assert(rope_cursor_index(rope_cursor_make(my_rope, index) == index));
 * ```
 * should always be true, assuming index is in the range *[0, length]* where
 * *length* is the length of the rope.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] cursor
 * \return the index of the cursor
 */
size_t
rope_cursor_index(rope_cursor const *cursor)
{
    return cursor->index;
}

/**
 * \brief Get the codepoint pointed to by a cursor
 * \ingroup cursor_functions
 *
 * Returns the codepoint at the cursor. Note that this is always a 32-bit
 * UTF-32 value, even though the ropes themselves use UTF-8 internally.
 * Returns `EOF` if the cursor points to beyond the end of a rope.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] cursor
 * \return the codepoint at the cursor, or `EOF`
 */
int32_t
rope_cursor_ref(rope_cursor const *cursor)
{
    if (cursor->path == NULL)
        return EOF;

    return rope_path_ref(cursor->path, cursor->offset);
}

