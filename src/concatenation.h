/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_CONCATENATION_H
#define ROPE_CONCATENATION_H

#include "rope_private.h"

__EXTERN_C_BEGIN_DECLS

rope           *rope_append_bang(rope *, rope *);

__EXTERN_C_END_DECLS

#endif

