/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdatomic.h>
#include <stdlib.h>

#include "cursor.h"
#include "memory.h"
#include "rope_private.h"

static atomic_size_t rope_object_counter = 0;

/**
 * \defgroup memory Destroying rope and cursor objects
 */

void *
rope_allocate(size_t size)
{
    void *object = calloc(size, 1);
#ifndef NDEBUG
    ++rope_object_counter;
#endif

    return object;
}

void *
rope_reallocate(void *ptr, size_t size)
{
    return realloc(ptr, size);
}

void
rope_deallocate(void *object)
{
    free(object);
#ifndef NDEBUG
    --rope_object_counter;
#endif
}

/**
 * \brief Return the number of objects created by **librope**.
 * \ingroup memory
 *
 * **librope** maintains a count of how many objects it has created which are
 * still on the heap (that is, which have not yet been freed). This function
 * is used for debugging purposes to make sure there are no memory leaks with
 * the internal reference count. It is intended to only be used to **librope**
 * developers, and will always return 0 if **librope** is compiled with
 * `NDEBUG`. However, you can also use it to check that you haven't forgotten
 * to release any \ref rope or \ref rope_cursor objects.
 *
 * **TODO:**
 * Add example usage.
 */
size_t
rope_get_object_count()
{
    return rope_object_counter;
}

