/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_BUILDER_H
#define ROPE_BUILDER_H

#include "node.h"

typedef struct rope_builder rope_builder;

__EXTERN_C_BEGIN_DECLS

rope_builder   *rope_builder_make(bool);
void            rope_builder_free();

void            rope_builder_add_codepoint(rope_builder *, int32_t);
rope           *rope_builder_extract_rope(rope_builder *);
rope           *rope_builder_extract_rope_bang(rope_builder *);

__EXTERN_C_END_DECLS

#endif
