/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>

#include "memory.h"
#include "tree.h"

struct rope_tree {
    rope_node node;
    rope_node *left;
    rope_node *right;
    size_t height;
};

static bool     left_leaning_p(rope_tree const *);
static bool     right_leaning_p(rope_tree const *);

rope_tree *
rope_tree_make(rope_node const *left, rope_node const *right)
{
    rope_tree *tree = rope_allocate(sizeof(*tree));
    tree->node.rc = 1;
    rope_node_set_length((rope_node *) tree,
                         rope_node_get_length(left) +
                         rope_node_get_length(right));
    rope_node_set_size((rope_node *) tree,
                       rope_node_get_size(left) +
                       rope_node_get_size(right));
    int left_height = rope_node_get_height(left);
    int right_height = rope_node_get_height(right);
    int max_height = left_height > right_height ? left_height : right_height;
    tree->height = 1 + max_height;
    tree->left = rope_node_rc_keep(left);
    tree->right = rope_node_rc_keep(right);

    return rope_tree_rebalance_bang(tree);
}

void
rope_tree_free(rope_tree *tree)
{
    rope_node_rc_drop(rope_node_get_left((rope_node *) tree));
    rope_node_rc_drop(rope_node_get_right((rope_node *) tree));
}

rope_node *
rope_tree_get_left(rope_tree const *tree)
{
    return tree->left;
}

rope_node *
rope_tree_get_right(rope_tree const *tree)
{
    return tree->right;
}

size_t
rope_tree_get_height(rope_tree const *tree)
{
    return tree->height;
}

rope_tree *
rope_tree_rebalance(rope_tree const *tree)
{
    rope_node_rc_keep((rope_node *) tree);

    while (left_leaning_p(tree))
        tree = rope_tree_rotate_right_bang((rope_tree *) tree);

    while (right_leaning_p(tree))
        tree = rope_tree_rotate_left_bang((rope_tree *) tree);

    return (rope_tree *) tree;
}

bool
left_leaning_p(rope_tree const *tree)
{
    return rope_node_get_height(tree->left) >
           (rope_node_get_height(tree->right) * 2 + 1);
}

bool
right_leaning_p(rope_tree const *tree)
{
    return rope_node_get_height(tree->right) >
           (rope_node_get_height(tree->left) * 2 + 1);
}

rope_tree *
rope_tree_rebalance_bang(rope_tree *tree)
{
    rope_tree *rebalanced_tree = rope_tree_rebalance(tree);
    rope_node_rc_drop((rope_node *) tree);

    return rebalanced_tree;
}

rope_tree *
rope_tree_rotate_left(rope_tree const *tree)
{
    rope_node *left_child = rope_node_get_left((rope_node *) tree);
    rope_node *right_child = rope_node_get_right((rope_node *) tree);
    rope_tree *new_left_child =
        rope_tree_make(left_child, rope_node_get_left(right_child));
    rope_tree *new_tree =
        rope_tree_make((rope_node *) new_left_child,
                       rope_node_get_right(right_child));
    rope_node_rc_drop((rope_node *) new_left_child);

    return new_tree;
}

rope_tree *
rope_tree_rotate_left_bang(rope_tree *tree)
{
    rope_tree *rotated_tree = rope_tree_rotate_left(tree);
    rope_node_rc_drop((rope_node *) tree);

    return rotated_tree;
}

rope_tree *
rope_tree_rotate_right(rope_tree const *tree)
{
    rope_node *left_child = rope_node_get_left((rope_node *) tree);
    rope_node *right_child = rope_node_get_right((rope_node *) tree);
    rope_tree *new_right_child =
        rope_tree_make(rope_node_get_right(left_child), right_child);
    rope_tree *new_tree =
        rope_tree_make(rope_node_get_left(left_child),
                       (rope_node *) new_right_child);
    rope_node_rc_drop((rope_node *) new_right_child);

    return new_tree;
}

rope_tree *
rope_tree_rotate_right_bang(rope_tree *tree)
{
    rope_tree *rotated_tree = rope_tree_rotate_right(tree);
    rope_node_rc_drop((rope_node *) tree);

    return rotated_tree;
}

void
rope_tree_extract(rope_tree const *tree, char *string, size_t offset)
{
    rope_node_extract(tree->left, string, offset);
    rope_node_extract(tree->right, string,
                      offset + rope_node_get_size(tree->left));
}

