/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>

#include "chunk.h"
#include "memory.h"
#include "path.h"
#include "tree.h"


static rope_path *rope_path_make_aux(rope_node const *, rope_path const *,
                                     size_t, bool);
static bool     index_in_left_child_p(rope_node const *, size_t);

rope_path *
rope_path_make_parent(rope_path const *parent, rope_node const *element,
                      bool right_child)
{
    rope_path *path = rope_allocate(sizeof(*path));
    path->rc = 1;
    path->element = rope_node_rc_keep((rope_node *) element);
    path->next = rope_path_rc_keep((rope_path *) parent);
    path->right_child = right_child;
    if (parent == NULL) {
        path->index = 0;
    } else if (right_child) {
        rope_node const *sibling = rope_node_get_left(parent->element);
        path->index = parent->index + rope_node_get_length(sibling);
    } else {
        path->index = parent->index;
    }

    return path;
}

rope_path *
rope_path_make(rope_node const *root, size_t index)
{
    return rope_path_make_aux(root, NULL, index, false);
}

rope_path *
rope_path_make_aux(rope_node const *node, rope_path const *parent,
                   size_t index, bool right_child)
{
    rope_path *path = rope_path_make_parent(parent, node, right_child);
    rope_path_rc_drop((rope_path *) parent);
    if (rope_node_leaf_p(node))
        return path;

    if (index_in_left_child_p(node, index)) {
        return rope_path_make_aux(rope_node_get_left(node), path, index, false);
    } else {
        index -= rope_node_get_length(rope_node_get_left(node));
        return rope_path_make_aux(rope_node_get_right(node), path, index, true);
    }
}

bool
index_in_left_child_p(rope_node const *node, size_t index)
{
    rope_node const *left = rope_node_get_left(node);
    size_t length = rope_node_get_length(left);

    return index < length;
}

bool
rope_path_came_from_left_child_p(rope_path const *previous)
{
    return !previous->right_child;
}

bool
rope_path_came_from_right_child_p(rope_path const *previous)
{
    return previous->right_child;
}

size_t
rope_path_offset(rope_path const *path, size_t index)
{
    rope_chunk *chunk = (rope_chunk *) path->element;

    return rope_chunk_offset(chunk, index - path->index);
}

size_t
rope_path_next_offset(rope_path const *path, size_t offset)
{
    rope_chunk *chunk = (rope_chunk *) path->element;

    return rope_chunk_next_offset(chunk, offset);
}

size_t
rope_path_prev_offset(rope_path const *path, size_t offset)
{
    rope_chunk *chunk = (rope_chunk *) path->element;

    return rope_chunk_prev_offset(chunk, offset);
}

bool
rope_path_end_p(rope_path const *path, size_t offset)
{
    rope_chunk *chunk = (rope_chunk *) path->element;

    return rope_chunk_end_p(chunk, offset);
}

int32_t
rope_path_ref(rope_path const *path, size_t offset)
{
    rope_chunk *chunk = (rope_chunk *) path->element;

    return rope_chunk_ref(chunk, offset);
}

rope_path *
rope_path_rc_keep(rope_path *path)
{
    if (path != NULL)
        path->rc += 1;

    return path;
}

void
rope_path_rc_drop(rope_path *path)
{
    if (path == NULL)
        return;
    if (path->rc == 1) {
        rope_node_rc_drop(path->element);
        rope_path_rc_drop(path->next);
        rope_deallocate(path);
    } else {
        path->rc -= 1;
    }
}

