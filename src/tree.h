/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_TREE_H
#define ROPE_TREE_H

#include "node.h"

typedef struct rope_tree rope_tree;

__EXTERN_C_BEGIN_DECLS

rope_tree      *rope_tree_make(rope_node const *, rope_node const *);
void            rope_tree_free(rope_tree *);
rope_node      *rope_tree_get_left(rope_tree const *);
rope_node      *rope_tree_get_right(rope_tree const *);
size_t          rope_tree_get_height(rope_tree const *);
rope_tree      *rope_tree_rebalance(rope_tree const *);
rope_tree      *rope_tree_rebalance_bang(rope_tree *);
rope_tree      *rope_tree_rotate_left(rope_tree const *);
rope_tree      *rope_tree_rotate_left_bang(rope_tree *);
rope_tree      *rope_tree_rotate_right(rope_tree const *);
rope_tree      *rope_tree_rotate_right_bang(rope_tree *);
void            rope_tree_extract(rope_tree const *, char *, size_t);

__EXTERN_C_END_DECLS

#endif
