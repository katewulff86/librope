/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include "cursor_construction.h"
#include "rope_private.h"

/**
 * \brief Create a cursor for a rope
 * \ingroup cursor_functions
 *
 * Create a new cursor to point to the `index`th codepoint in the rope. If the
 * `index` is outside of the range *[0, length)*, where *length*
 * is the length of the rope, a cursor to just past the end of the rope will
 * be returned.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope the rope from which to create a cursor
 * \param[in] index the index into the rop
 * \return a cursor
 */
rope_cursor *
rope_cursor_make(rope const *rope, size_t index)
{
    return rope_cursor_make_from_root(rope->root, index);
}

rope_cursor *
rope_cursor_make_from_root(rope_node *node, size_t index)
{
    if (index >= rope_node_get_length(node))
        return rope_cursor_make_eof_from_root(node);

    rope_path *path = rope_path_make(node, index);
    size_t offset = rope_path_offset(path, index);
    rope_cursor *cursor = rope_cursor_make_raw(path, node, index, offset);
    rope_path_rc_drop(path);

    return cursor;
}

rope_cursor *
rope_cursor_make_eof_from_root(rope_node *node)
{
    size_t index = rope_node_get_length(node);

    return rope_cursor_make_raw(NULL, node, index, 0);
}

/**
 * \brief Create a cursor for the start of a rope
 * \ingroup cursor_functions
 *
 * Create a new cursor to point to the start of a given rope.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope the rope from which to create a cursor
 * \return a cursor
 */
rope_cursor *
rope_cursor_make_start(rope const *rope)
{
    return rope_cursor_make(rope, 0);
}

/**
 * \brief Create a cursor for the end of a rope
 * \ingroup cursor_functions
 *
 * Create a new cursor to point to one past the end of a rope. Functions which
 * take two cursors to select a range, such as
 * \ref rope_extract_cursor, will select a range from
 * *[start, end)*. That is, the start is inclusive and the end is exclusive.
 * The cursor returned by this function is just past the end, so by using it
 * you can select everything up to the end of a given rope.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope the rope from which to create a cursor
 * \return a cursor
 */
rope_cursor *
rope_cursor_make_end(rope const *rope)
{
    return rope_cursor_make(rope, rope_length(rope));
}

rope_cursor *
rope_cursor_clone(rope_cursor const *cursor)
{
    rope_path *path = cursor->path;
    rope_node *root = cursor->root;
    size_t index = cursor->index;
    size_t offset = cursor->offset;

    return rope_cursor_make_raw(path, root, index, offset);
}

