/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include "rope_private.h"

/**
 * \brief See if two ropes are equal (case sensitive)
 * \ingroup rope_comparison
 *
 * Compare two ropes to see if they contain the same codepoints. This check
 * is case sensitive.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] first first rope
 * \param[in] second second rope
 * \return `true` if they are the same
 */
bool
rope_eq_p(rope const *first, rope const *second)
{
    if (first->root == second->root)
        return true;
    if (rope_length(first) != rope_length(second))
        return false;

    rope_cursor *first_cursor = rope_cursor_make_start(first);
    rope_cursor *second_cursor = rope_cursor_make_start(second);
    bool result = true;
    while (!rope_cursor_end_p(first_cursor)) {
        int32_t first_codepoint = rope_cursor_ref(first_cursor);
        int32_t second_codepoint = rope_cursor_ref(second_cursor);
        if (first_codepoint != second_codepoint) {
            result = false;
            break;
        }
        first_cursor = rope_cursor_next_bang(first_cursor);
        second_cursor = rope_cursor_next_bang(second_cursor);
    }

    rope_cursor_free(first_cursor); rope_cursor_free(second_cursor);

    return result;
}

