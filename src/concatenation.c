/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include "concatenation.h"
#include "tree.h"

rope *
rope_append(rope const *left, rope const *right)
{
    /* Get simple cases out of the way. */
    if (rope_null_p(left))
        return rope_clone(right);
    if (rope_null_p(right))
        return rope_clone(left);

    rope_tree *root = rope_tree_make(left->root, right->root);

    return rope_make_node_bang((rope_node *) root);
}

rope *
rope_append_bang(rope *left, rope *right)
{
    rope *appended_rope = rope_append(left, right);
    rope_free(left);
    rope_free(right);

    return appended_rope;
}

