/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_PRIVATE_H
#define ROPE_PRIVATE_H

#include <stdatomic.h>

#include "node.h"

/**
 * \struct rope
 * \ingroup datatypes
 * \headerfile rope.h
 * \brief Opaque rope data type.
 *
 * Create with a \ref rope_constructors "constructor", and free with
 * \ref rope_free. The most common way to make a rope is with
 * \ref rope_make_string.
 *
 * ```c
 * #include <rope.h>
 *
 * int main() {
 *     rope *my_rope = rope_make_string("Hello, World!");
 *     do_stuff_with_rope(my_rope);
 *     rope_free(my_rope);
 *
 *     return 0;
 * }
 * ```
 */
struct rope {
    rope_node *root; ///< root node of the tree
};

/**
 * \defgroup datatypes Datatypes in **librope**
 */

/**
 * \defgroup internal_datatypes Internal datatypes in **librope**
 */

__EXTERN_C_BEGIN_DECLS

rope           *rope_make_node(rope_node *);
rope           *rope_make_node_bang(rope_node *);
rope           *rope_make_data(uint8_t *, size_t, size_t);
rope_node      *rope_get_root(rope const *);
void            rope_set_root(rope *, rope_node *);
size_t          rope_get_size(rope const *);

__EXTERN_C_END_DECLS

#endif
