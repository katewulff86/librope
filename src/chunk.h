/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_CHUNK_H
#define ROPE_CHUNK_H

#include "node.h"

#define MAX_CHUNK_LENGTH 64
#define MAX_BYTES_PER_CODEPOINT 4
#define MAX_CHUNK_SIZE (MAX_CHUNK_LENGTH * MAX_BYTES_PER_CODEPOINT)

typedef struct rope_chunk rope_chunk;

__EXTERN_C_BEGIN_DECLS

rope_chunk     *rope_chunk_make(uint8_t const *, size_t, size_t);
void            rope_chunk_free(rope_chunk *);
void            rope_chunk_bifurcate(rope_chunk const *, size_t,
                                     rope_chunk **, rope_chunk **);
rope_chunk     *rope_chunk_prefix(rope_chunk const *, size_t);
rope_chunk     *rope_chunk_suffix(rope_chunk const *, size_t);
void            rope_chunk_extract(rope_chunk const *, char *, size_t);
size_t          rope_chunk_offset(rope_chunk const *, size_t);
size_t          rope_chunk_next_offset(rope_chunk const *, size_t);
size_t          rope_chunk_prev_offset(rope_chunk const *, size_t);
bool            rope_chunk_end_p(rope_chunk const *, size_t);
int32_t         rope_chunk_ref(rope_chunk const *, size_t);

__EXTERN_C_END_DECLS

#endif
