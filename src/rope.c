/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include "chunk.h"
#include "memory.h"
#include "rope_private.h"

/**
 * \defgroup internal_rope_constructors Internal rope construction functions
 */

/**
 * \brief Create a new rope from an existing node.
 * \ingroup internal_rope_constructors
 *
 * Create a new rope by linking it to an existing node.
 *
 * \param[in] node node to be linked
 * \return a new rope
 */
rope *
rope_make_node(rope_node *node)
{
    rope *rope = rope_allocate(sizeof(*rope));
    rope_set_root(rope, node);

    return rope;
}

/* TODO
 * give this function a better name
 */
/**
 * \brief Destructively create a new rope from an existing node.
 * \ingroup internal_rope_constructors
 *
 * Create a new rope by linking it to an existing node. The node's reference
 * count will not be increased.
 *
 * \param[in] node node to be linked
 * \return a new rope
 */
rope *
rope_make_node_bang(rope_node *node)
{
    rope *rope = rope_allocate(sizeof(*rope));
    rope->root = (rope_node *) node;

    return rope;
}

rope *
rope_make_data(uint8_t *data, size_t length, size_t size)
{
    rope_chunk *chunk = rope_chunk_make(data, length, size);

    return rope_make_node_bang((rope_node *) chunk);
}

rope_node *
rope_get_root(rope const *rope)
{
    return rope_node_rc_keep(rope->root);
}

void
rope_set_root(rope *rope, rope_node *root)
{
    rope_node_rc_drop(rope->root);
    rope->root = rope_node_rc_keep(root);
}

size_t
rope_get_size(rope const *rope)
{
    return rope_node_get_size(rope->root);
}

/**
 * \brief Destroy a rope
 * \ingroup memory
 *
 * Destroy a rope and free its associated memory. Any data reachable from only
 * this rope will also be freed.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope to be destroyed
 */
void
rope_free(rope *rope)
{
    if (rope->root != NULL)
        rope_node_rc_drop(rope->root);
    rope_deallocate(rope);
}

