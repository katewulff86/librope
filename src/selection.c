/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "cursor_splitting.h"
#include "rope_private.h"

/**
 * \brief Extract a string from a rope
 * \ingroup rope_selection
 *
 * Copy the contents of a rope into a string. It is the responsibility of the
 * caller to free its storage.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope to be extracted
 * \return extracted string
 */
char *
rope_extract(rope const *rope)
{
    char *string = malloc(sizeof(*string) * (rope_get_size(rope) + 1));
    size_t offset = 0;
    rope_node_extract(rope->root, string, offset);
    string[rope_get_size(rope)] = '\0';

    return string;
}

/**
 * \brief Extract a string from a subrope of a rope
 * \ingroup rope_selection
 *
 * Copy the contents of a subrope of a rope into a string. It is the
 * responsibility of the caller to free its storage. The return value is
 * unspecified if `end` < `start`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope to be extracted
 * \param[in] start starting index of range (inclusive)
 * \param[in] end ending index of range (exclusive)
 * \return extracted string
 */
char *
rope_extract_start_end(rope const *rope, size_t start, size_t end)
{
    if (end < start)
        return NULL;

    rope_cursor *start_cursor = rope_cursor_make(rope, start);
    rope_cursor *end_cursor = rope_cursor_make(rope, end);

    char *result = rope_extract_cursor(start_cursor, end_cursor);
    rope_cursor_free(start_cursor);
    rope_cursor_free(end_cursor);

    return result;
}

/**
 * \brief Extract a string from a subrope of a rope
 * \ingroup rope_selection
 *
 * Copy the contents of a subrange of a rope into a string. It is the
 * responsibility of the caller to free its storage. The return value is
 * unspecified if they point to different ropes, or if `end` < `start`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] start starting cursor of range (inclusive)
 * \param[in] end ending cursor of range (exclusive)
 * \return extracted string
 */
char *
rope_extract_cursor(rope_cursor const *start, rope_cursor const *end)
{
    rope *subrope = rope_subrope_cursor(start, end);
    if (subrope == NULL)
        return NULL;

    char *result = rope_extract(subrope);
    rope_free(subrope);

    return result;
}

/**
 * \brief Get length of a rope
 * \ingroup rope_selection
 *
 * Get the length of a rope in codepoints.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope a rope
 * \return length of the rope
 */
size_t
rope_length(rope const *rope)
{
    return rope_node_get_length(rope->root);
}

/**
 * \brief Get a codepoint from a rope
 * \ingroup rope_selection
 *
 * Returns the codepoint at the specified index from a given rope. Returns
 * `EOF` if the index is out of range.
 *
 * **TODO:**
 * Add example usage.
 *
 * \return codepoint
 */
int32_t
rope_ref(rope const *rope, size_t index)
{
    rope_cursor *cursor = rope_cursor_make(rope, index);
    int32_t codepoint = rope_cursor_ref(cursor);
    rope_cursor_free(cursor);

    return codepoint;
}

/**
 * \brief Get a subrope of a rope
 * \ingroup rope_selection
 *
 * Get a subrope of a rope from `start` (inclusive) to an `end`
 * (exclusive). That is, return a rope with the beginning removed
 * (up to `start`) and the end removed (from `end`). The return value is
 * unspecified if `end` < `start`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope a rope
 * \param[in] start starting index of range (inclusive)
 * \param[in] end ending index of range (exclusive)
 * \return a subrope
 */
rope *
rope_subrope_start_end(rope const *r, size_t start, size_t end)
{
    if (end < start)
        return NULL;

    rope_cursor *start_cursor = rope_cursor_make(r, start);
    rope_cursor *end_cursor = rope_cursor_make(r, end);
    rope *subrope = rope_subrope_cursor(start_cursor, end_cursor);
    rope_cursor_free(start_cursor);
    rope_cursor_free(end_cursor);

    return subrope;
}

/**
 * \brief Get a subrope of a rope
 * \ingroup rope_selection
 *
 * Get a subrope of a rope from a `start` cursor (inclusive) to an `end` cursor
 * (exclusive). That is, return a rope with the beginning removed
 * (up to `start`) and the end removed (from `end`). The return value is
 * unspecified if they point to different ropes, or if `end` < `start`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] start starting cursor of range (inclusive)
 * \param[in] end ending cursor or range (exclusive)
 * \return a subrope
 */
rope *
rope_subrope_cursor(rope_cursor const *start, rope_cursor const *end)
{
    if (!rope_cursor_le_p(start, end))
        return NULL;

    rope *prefix = rope_cursor_prefix(end);
    size_t start_index = rope_cursor_index(start);
    rope_cursor *start_prefix = rope_cursor_make(prefix, start_index);
    rope *subrope = rope_cursor_suffix(start_prefix);
    rope_free(prefix);
    rope_cursor_free(start_prefix);

    return subrope;
}

/**
 * \brief Take the start of a rope
 * \ingroup rope_selection
 *
 * Returns the first `length` codepoints in a rope. The return value is
 * unspecified if the `length` given is longer than the `rope`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope to take codepoints from
 * \param[in] length how many codepoints to take
 * \return prefix of rope
 */
rope *
rope_take(rope const *rope_, size_t length)
{
    rope_cursor *cursor = rope_cursor_make(rope_, length);
    rope *result = rope_cursor_prefix(cursor);
    rope_cursor_free(cursor);

    return result;
}

/**
 * \brief Drop the start of a rope
 * \ingroup rope_selection
 *
 * Returns a rope with the first `length` codepoints dropped. The return value
 * is unspecified if the `length` given is longer than the `rope`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope from which to drop codepoints
 * \param[in] length how many codepoints to drop
 * \return prefix of rope
 */
rope *
rope_drop(rope const *rope_, size_t length)
{
    rope_cursor *cursor = rope_cursor_make(rope_, length);
    rope *result = rope_cursor_suffix(cursor);
    rope_cursor_free(cursor);

    return result;
}

/**
 * \brief Take the end of a rope
 * \ingroup rope_selection
 *
 * Returns the last `length` codepoints in a rope. The return value is
 * unspecified if the `length` given is longer than the `rope`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope to take codepoints from
 * \param[in] length how many codepoints to take
 * \return prefix of rope
 */
rope *
rope_take_right(rope const *rope_, size_t length)
{
    if (length > rope_length(rope_))
        return NULL;

    size_t index = rope_length(rope_) - length;
    rope_cursor *cursor = rope_cursor_make(rope_, index);
    rope *result = rope_cursor_suffix(cursor);
    rope_cursor_free(cursor);

    return result;
}

/**
 * \brief Drop the end of a rope
 * \ingroup rope_selection
 *
 * Returns a rope with the last `length` codepoints in the given rope dropped.
 * The return value is unspecified if the `length` given is longer than the
 * `rope`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope to drop codepoints from
 * \param[in] length how many codepoints to drop
 * \return prefix of rope
 */
rope *
rope_drop_right(rope const *rope_, size_t length)
{
    if (length > rope_length(rope_))
        return NULL;

    size_t index = rope_length(rope_) - length;
    rope_cursor *cursor = rope_cursor_make(rope_, index);
    rope *result = rope_cursor_prefix(cursor);
    rope_cursor_free(cursor);

    return result;
}

/**
 * \brief Pad a rope on the left
 * \ingroup rope_selection
 *
 * Returns a rope of length `length`, padded on the left with the given
 * codepoint. If the provided `length` is shorter than the rope, the rope is
 * truncated on the left.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope to pad
 * \param[in] length length of resulting rope
 * \param[in] codepoint codepoint with which to pad
 * \return padded rope
 */
rope *
rope_pad(rope const *rope_, size_t length, int32_t codepoint)
{
    if (length <= rope_length(rope_))
        return rope_drop(rope_, rope_length(rope_) - length);

    rope *pad = rope_make_repeating(length - rope_length(rope_), codepoint);
    rope *joined = rope_append(pad, rope_);
    rope_free(pad);

    return joined;
}

/**
 * \brief Pad a subrope on the left
 * \ingroup rope_selection
 *
 * Returns a rope of length `length`, padded on the left with the given
 * codepoint. If the provided `length` is shorter than the subrope, the subrope
 * is truncated on the left.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope to pad
 * \param[in] length length of resulting rope
 * \param[in] codepoint codepoint with which to pad
 * \param[in] start start of range of subrope (inclusive)
 * \param[in] end end of range of subrope (exclusive)
 * \return padded rope
 */
rope *
rope_pad_start_end(rope const *rope_, size_t length, int32_t codepoint,
                   size_t start, size_t end)
{
    rope *subrope = rope_subrope_start_end(rope_, start, end);
    if (subrope == NULL)
        return NULL;

    rope *result = rope_pad(subrope, length, codepoint);
    rope_free(subrope);

    return result;
}

/**
 * \brief Pad a subrope on the left
 * \ingroup rope_selection
 *
 * Returns a rope of length `length`, padded on the left with the given
 * codepoint. If the provided `length` is shorter than the subrope, the subrope
 * is truncated on the left.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] length length of resulting rope
 * \param[in] codepoint codepoint with which to pad
 * \param[in] start start cursor of range of subrope (inclusive)
 * \param[in] end end cursor of range of subrope (exclusive)
 * \return padded rope
 */
rope *
rope_pad_cursor(size_t length, int32_t codepoint,
                rope_cursor const *start, rope_cursor const *end)
{
    rope *subrope = rope_subrope_cursor(start, end);
    if (subrope == NULL)
        return NULL;

    rope *result = rope_pad(subrope, length, codepoint);
    rope_free(subrope);

    return result;
}

/**
 * \brief Pad a rope on the right
 * \ingroup rope_selection
 *
 * Returns a rope of length `length`, padded on the right with the given
 * codepoint. If the provided `length` is shorter than the rope, the rope is
 * truncated on the right.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope to pad
 * \param[in] length length of resulting rope
 * \param[in] codepoint codepoint with which to pad
 * \return padded rope
 */
rope *
rope_pad_right(rope const *rope_, size_t length, int32_t codepoint)
{
    if (length <= rope_length(rope_))
        return rope_drop_right(rope_, rope_length(rope_) - length);

    rope *pad = rope_make_repeating(length - rope_length(rope_), codepoint);
    rope *joined = rope_append(rope_, pad);
    rope_free(pad);

    return joined;
}

/**
 * \brief Pad a subrope on the right
 * \ingroup rope_selection
 *
 * Returns a rope of length `length`, padded on the right with the given
 * codepoint. If the provided `length` is shorter than the subrope, the subrope
 * is truncated on the right.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope to pad
 * \param[in] length length of resulting rope
 * \param[in] codepoint codepoint with which to pad
 * \param[in] start start of range of subrope (inclusive)
 * \param[in] end end of range of subrope (exclusive)
 * \return padded rope
 */
rope *
rope_pad_right_start_end(rope const *rope_, size_t length, int32_t codepoint,
                         size_t start, size_t end)
{
    rope *subrope = rope_subrope_start_end(rope_, start, end);
    if (subrope == NULL)
        return NULL;

    rope *result = rope_pad_right(subrope, length, codepoint);
    rope_free(subrope);

    return result;
}

/**
 * \brief Pad a subrope on the right
 * \ingroup rope_selection
 *
 * Returns a rope of length `length`, padded on the right  with the given
 * codepoint. If the provided `length` is shorter than the subrope, the subrope
 * is truncated on the right.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] length length of resulting rope
 * \param[in] codepoint codepoint with which to pad
 * \param[in] start start cursor of range of subrope (inclusive)
 * \param[in] end end cursor of range of subrope (exclusive)
 * \return padded rope
 */
rope *
rope_pad_right_cursor(size_t length, int32_t codepoint,
                      rope_cursor const *start, rope_cursor const *end)
{
    rope *subrope = rope_subrope_cursor(start, end);
    if (subrope == NULL)
        return NULL;

    rope *result = rope_pad_right(subrope, length, codepoint);
    rope_free(subrope);

    return result;
}

