/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include "cursor_splitting.h"
#include "path_splitting.h"
#include "rope_private.h"

void
rope_cursor_bifurcate(rope_cursor const *cursor, rope **left, rope **right)
{
    rope_node *left_node = NULL;
    rope_node *right_node = NULL;
    rope_path_bifurcate(cursor->path, cursor->offset, &left_node, &right_node);
    *left = rope_make_node_bang(left_node);
    *right = rope_make_node_bang(right_node);
}

rope *
rope_cursor_prefix(rope_cursor const *cursor)
{
    if (rope_cursor_start_p(cursor))
        return rope_make_empty();
    if (rope_cursor_end_p(cursor))
        return rope_make_node(cursor->root);

    rope_node *node = rope_path_prefix(cursor->path, cursor->offset);

    return rope_make_node_bang(node);
}

rope *
rope_cursor_suffix(rope_cursor const *cursor)
{
    if (rope_cursor_start_p(cursor))
        return rope_make_node(cursor->root);
    if (rope_cursor_end_p(cursor))
        return rope_make_empty();

    rope_node *node = rope_path_suffix(cursor->path, cursor->offset);

    return rope_make_node_bang(node);
}

