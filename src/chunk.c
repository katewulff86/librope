/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <string.h>
#include <utf8proc.h>

#include "chunk.h"
#include "memory.h"
#include "misc.h"

struct rope_chunk {
    rope_node node;
    uint8_t const *data;
};

rope_chunk *
rope_chunk_make(uint8_t const *data, size_t length, size_t size)
{
    rope_chunk *chunk = rope_allocate(sizeof(*chunk));
    chunk->node.rc = 1;
    chunk->data = data;
    rope_node_set_length((rope_node *) chunk, length);
    rope_node_set_size((rope_node *) chunk, size);
    rope_node_set_leaf((rope_node *) chunk, true);

    return chunk;
}

void
rope_chunk_free(rope_chunk *chunk)
{
    rope_deallocate((uint8_t *) chunk->data);
}

void
rope_chunk_bifurcate(rope_chunk const *chunk, size_t index,
                     rope_chunk **left, rope_chunk **right)
{
    *left = rope_chunk_prefix(chunk, index);
    *right = rope_chunk_suffix(chunk, index);
}

rope_chunk *
rope_chunk_prefix(rope_chunk const *chunk, size_t index)
{
    if (index == 0)
        return NULL;
    size_t left_size = index;
    uint8_t *left_data = rope_allocate(left_size);
    memcpy(left_data, chunk->data, left_size);
    size_t left_codepoints = rope_count_codepoints(left_data, left_size);

    return rope_chunk_make(left_data, left_codepoints, left_size);
}

rope_chunk  *
rope_chunk_suffix(rope_chunk const *chunk, size_t index)
{
    if (index == 0) {
        return (rope_chunk *) rope_node_rc_keep((rope_node *) chunk);
    }
    size_t right_size = rope_node_get_size((rope_node *) chunk) - index;
    uint8_t *right_data = rope_allocate(right_size);
    memcpy(right_data, &chunk->data[index], right_size);
    size_t right_codepoints = rope_count_codepoints(right_data, right_size);

    return rope_chunk_make(right_data, right_codepoints, right_size);
}

void
rope_chunk_extract(rope_chunk const *chunk, char *string, size_t offset)
{
    memcpy(&string[offset], chunk->data,
           rope_node_get_size((rope_node *) chunk));
}

size_t
rope_chunk_offset(rope_chunk const *chunk, size_t index)
{
    return rope_codepoint_offset(chunk->data, index);
}

size_t
rope_chunk_next_offset(rope_chunk const *chunk, size_t offset)
{
    uint8_t byte = chunk->data[offset];
    size_t codepoint_length = utf8proc_utf8class[byte];

    return offset + codepoint_length;
}

size_t
rope_chunk_prev_offset(rope_chunk const *chunk, size_t offset)
{
    int32_t codepoint;
    ptrdiff_t bytes;
    do {
        --offset;
        bytes = utf8proc_iterate(&chunk->data[offset], -1, &codepoint);
    } while (bytes < 0);

    return offset;
}

bool
rope_chunk_end_p(rope_chunk const *chunk, size_t offset)
{
    size_t next_offset = rope_chunk_next_offset(chunk, offset);
    size_t size = rope_node_get_size((rope_node *) chunk);

    return next_offset >= size;
}

int32_t
rope_chunk_ref(rope_chunk const *chunk, size_t offset)
{
    int32_t codepoint;
    utf8proc_iterate(&chunk->data[offset], -1, &codepoint);

    return codepoint;
}

