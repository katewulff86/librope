/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <string.h>

#include <utf8proc.h>

#include "builder.h"
#include "chunk.h"
#include "concatenation.h"
#include "memory.h"
#include "rope_private.h"

/**
 * \struct rope_builder
 * \ingroup internal_datatypes
 * \brief Build a rope, one codepoint at a time.
 *
 * `rope_builder`s are used by some rope constructors to be able to construct
 * a rope one codepoint at a time. Codepoints are added to an internal buffer,
 * and when the buffer gets too big it is converted into a \ref rope_chunk and
 * added to the growing rope.
 *
 * `rope_builder`s support growing a rope forwards (left-to-right) or
 * backwards (right-to-left).
 *
 * When you have finished adding codepoints, use \ref rope_builder_extract_rope
 * to extract the created rope.
 */
struct rope_builder {
    rope           *rope;
    uint8_t        *buffer;
    size_t          length;
    size_t          size;
    size_t          index;
    bool            forwards;
};

static void     append_codepoint(rope_builder *, int32_t);
static void     prepend_codepoint(rope_builder *, int32_t);
static void     add_buffer_to_rope_and_reset(rope_builder *);
static void     add_buffer_to_rope(rope_builder *);
static void     shrink_buffer(rope_builder *);
static void     move_buffer_to_start(rope_builder *);
static void     reset_buffer(rope_builder *);
static void     append_buffer_to_rope_bang(rope_builder *);
static rope    *buffer_to_rope(rope_builder *);
static void     prepend_buffer_to_rope_bang(rope_builder *);

rope_builder *
rope_builder_make(bool forwards)
{
    rope_builder *builder = rope_allocate(sizeof(*builder));
    builder->rope = rope_make_empty();
    builder->forwards = forwards;
    reset_buffer(builder);

    return builder;
}

void
rope_builder_free(rope_builder *builder)
{
    if (builder->rope != NULL)
        rope_free(builder->rope);
    if (builder->buffer != NULL)
        rope_deallocate(builder->buffer);
    rope_deallocate(builder);
}

void
rope_builder_add_codepoint(rope_builder *builder, int32_t codepoint)
{
    if (builder->forwards)
        append_codepoint(builder, codepoint);
    else
        prepend_codepoint(builder, codepoint);

    if (builder->length >= MAX_CHUNK_LENGTH)
        add_buffer_to_rope_and_reset(builder);
}

void
append_codepoint(rope_builder *builder, int32_t codepoint)
{
    uint8_t codepoint_data[4];
    size_t codepoint_length = utf8proc_encode_char(codepoint, codepoint_data);
    memcpy(&builder->buffer[builder->index], codepoint_data, codepoint_length);

    builder->size += codepoint_length;
    builder->length += 1;
    builder->index += codepoint_length;
}

void
prepend_codepoint(rope_builder *builder, int32_t codepoint)
{
    uint8_t codepoint_data[4];
    size_t codepoint_length = utf8proc_encode_char(codepoint, codepoint_data);
    size_t copy_index = builder->index - codepoint_length;
    memcpy(&builder->buffer[copy_index], codepoint_data, codepoint_length);

    builder->size += codepoint_length;
    builder->length += 1;
    builder->index -= codepoint_length;
}

void
add_buffer_to_rope_and_reset(rope_builder *builder)
{
    add_buffer_to_rope(builder);
    reset_buffer(builder);
}

void
add_buffer_to_rope(rope_builder *builder)
{
    shrink_buffer(builder);
    if (builder->forwards)
        append_buffer_to_rope_bang(builder);
    else
        prepend_buffer_to_rope_bang(builder);
}

void
shrink_buffer(rope_builder *builder)
{
    if (!builder->forwards)
        move_buffer_to_start(builder);
    rope_reallocate(builder->buffer, builder->size);
}

/* When prepending codepoints, they will be added right to left in the buffer.
 * That is, the first codepoint added will be at the end of the buffer, the next
 * to the left of it, and so on. When we want to convert the buffer into a chunk
 * we need to move all the data in the buffer to the start of the buffer. We can
 * then \ref rope_reallocate to shrink it down to the correct size.
 */
void
move_buffer_to_start(rope_builder *builder)
{
    size_t start_index = MAX_CHUNK_SIZE - builder->size;
    memmove(builder->buffer, &builder->buffer[start_index], builder->size);
}

void
reset_buffer(rope_builder *builder)
{
    builder->buffer = rope_allocate(sizeof(*builder->buffer) * MAX_CHUNK_SIZE);
    builder->length = 0;
    builder->size = 0;

    if (builder->forwards)
        builder->index = 0;
    else
        builder->index = MAX_CHUNK_SIZE;
}

void
append_buffer_to_rope_bang(rope_builder *builder)
{
    rope *buffer_rope = buffer_to_rope(builder);
    builder->rope = rope_append_bang(builder->rope, buffer_rope);
}

rope *
buffer_to_rope(rope_builder *builder)
{
    return rope_make_data(builder->buffer, builder->length, builder->size);
}

void
prepend_buffer_to_rope_bang(rope_builder *builder)
{
    rope *buffer_rope = buffer_to_rope(builder);
    builder->rope = rope_append_bang(buffer_rope, builder->rope);
}

rope *
rope_builder_extract_rope(rope_builder *builder)
{
    if (builder->length > 0) {
        add_buffer_to_rope(builder);
        builder->buffer = NULL;
    }

    rope *extracted_rope = builder->rope;
    builder->rope = NULL;

    return extracted_rope;
}

rope *
rope_builder_extract_rope_bang(rope_builder *builder)
{
    rope *result = rope_builder_extract_rope(builder);
    rope_builder_free(builder);

    return result;
}

