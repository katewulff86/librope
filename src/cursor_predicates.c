/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include "cursor_predicates.h"

/**
 * \brief Compare two cursors to see if they are equal
 * \ingroup cursor_functions
 *
 * Compare two cursors to see if they are equal. That is, if they point to the
 * same codepoint in the same rope. The return value is unspecified if they
 * point to different ropes.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] a first cursor into a rope
 * \param[in] b second cursor into a rope
 * \return `true` if the cursors point to the same codepoint, `false` otherwise
 */
bool
rope_cursor_eq_p(rope_cursor const *a, rope_cursor const *b)
{
    if (!rope_cursor_same_rope(a, b))
        return false;
    return a->index == b->index;
}

/**
 * \brief Compare two cursors to see if one is less than the other
 * \ingroup cursor_functions
 *
 * Compare two cursors to see if the first is less than the second. That is, if
 * the first points to a codepoint before the second in the same rope. The
 * return value is unspecified if they point to different ropes.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] a first cursor into a rope
 * \param[in] b second cursor into a rope
 * \return `true` if the first points to an earlier codepoint than the second
 */
bool
rope_cursor_lt_p(rope_cursor const *a, rope_cursor const *b)
{
    if (!rope_cursor_same_rope(a, b))
        return false;
    return a->index < b->index;
}

/**
 * \brief Compare two cursors to see if one is less than or equal to the other
 * \ingroup cursor_functions
 *
 * Compare two cursors to see if the first is less than or equal to the second.
 * That is, if the first points to the same or a codepoint before the second in
 * the same rope. The return value is unspecified if they point to different
 * ropes.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] a first cursor into a rope
 * \param[in] b second cursor into a rope
 * \return `true` if the first points to the same or earlier codepoint than the
 * second
 */
bool
rope_cursor_le_p(rope_cursor const *a, rope_cursor const *b)
{
    if (!rope_cursor_same_rope(a, b))
        return false;
    return a->index <= b->index;
}

/**
 * \brief Compare two cursors to see if one is greater than the other
 * \ingroup cursor_functions
 *
 * Compare two cursors to see if the first is greater than the second. That is,
 * if the first points to a later codepoint than the second in the same rope.
 * The return value is unspecified if they point to different ropes.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] a first cursor into a rope
 * \param[in] b second cursor into a rope
 * \return `true` if the first points to a later codepoint than the second
 */
bool
rope_cursor_gt_p(rope_cursor const *a, rope_cursor const *b)
{
    if (!rope_cursor_same_rope(a, b))
        return false;
    return a->index > b->index;
}

/**
 * \brief Compare two cursors to see if one is greater than or equal to the
 * other
 * \ingroup cursor_functions
 *
 * Compare two cursors to see if the first is greater than or equal to the
 * second. That is, if the first points to the same codepoint or a later
 * codepoint than the second in the same rope. The return value is unspecified
 * if they point to different ropes.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] a first cursor into a rope
 * \param[in] b second cursor into a rope
 * \return `true` if the first points to the same or later codepoint than the
 * second
 */
bool
rope_cursor_ge_p(rope_cursor const *a, rope_cursor const *b)
{
    if (!rope_cursor_same_rope(a, b))
        return false;
    return a->index >= b->index;
}

/**
 * \brief Check if a cursor is at the start of a rope
 * \ingroup cursor_functions
 *
 * Returns `true` if a cursor is at the start of a rope, and false otherwise.
 * ```c
 * assert(rope_cursor_start_p(rope_cursor_make_start(my_rope)));
 * ```
 * should always succeed.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] cursor
 * \return `true` if the cursor is at the start of a rope, false otherwise.
 */
bool
rope_cursor_start_p(rope_cursor const *cursor)
{
    return cursor->index == 0;
}

/**
 * \brief Check if a cursor is at the end of a rope
 * \ingroup cursor_functions
 *
 * Returns `true` if a cursor is at the end of a rope, and false otherwise.
 * ```c
 * assert(rope_cursor_end_p(rope_cursor_make_end(my_rope)));
 * ```
 * should always succeed.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] cursor
 * \return `true` if the cursor is at the end of a rope, false otherwise.
 */
bool
rope_cursor_end_p(rope_cursor const *cursor)
{
    return cursor->index >= rope_node_get_length(cursor->root);
}

bool
rope_cursor_same_rope(rope_cursor const *a, rope_cursor const *b)
{
    return a->root == b->root;
}

