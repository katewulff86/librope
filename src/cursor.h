/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_CURSOR_H
#define ROPE_CURSOR_H

#include "path.h"

/**
 * \struct rope_cursor
 * \ingroup datatypes
 * \headerfile rope.h
 * \brief Opaque cursor data type.
 */
struct rope_cursor {
    rope_path *path;
    rope_node *root;
    size_t index;
    size_t offset;
};

__EXTERN_C_BEGIN_DECLS

rope_cursor    *rope_cursor_make_raw(rope_path *, rope_node *, size_t, size_t);

__EXTERN_C_END_DECLS

#endif

