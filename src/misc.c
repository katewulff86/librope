/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <utf8proc.h>

#include "misc.h"

inline size_t
rope_codepoint_offset(uint8_t const *data, size_t index)
{
    size_t offset = 0;
    for (; index > 0; --index) {
        size_t bytes = utf8proc_utf8class[data[offset]];
        offset += bytes;
    }

    return offset;
}

size_t
rope_count_codepoints(uint8_t const *data , size_t size)
{
    size_t codepoints = 0;
    for (size_t offset = 0; offset < size;) {
        size_t bytes = utf8proc_utf8class[data[offset]];
        offset += bytes;
        ++codepoints;
    }

    return codepoints;
}

int32_t
extract_codepoint_and_advance(char const **string)
{
    while (*string[0] != '\0') {
        int32_t codepoint;
        int bytes_read = utf8proc_iterate((uint8_t *) *string, -1, &codepoint);
        if (bytes_read < 0) {
            *string += 1;
        } else {
            *string += bytes_read;

            return codepoint;
        }
    }

    return EOF;
}
