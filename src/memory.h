/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_MEMORY_H
#define ROPE_MEMORY_H

#include <stddef.h>

#include "rope.h"

__EXTERN_C_BEGIN_DECLS

void           *rope_allocate(size_t);
void           *rope_reallocate(void *, size_t);
void            rope_deallocate(void *);

__EXTERN_C_END_DECLS

#endif
