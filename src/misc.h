/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_MISC_H
#define ROPE_MISC_H

#include <stdint.h>

#include "rope_private.h"

__EXTERN_C_BEGIN_DECLS

size_t          rope_codepoint_offset(uint8_t const *, size_t);
size_t          rope_count_codepoints(uint8_t const *, size_t);
int32_t         extract_codepoint_and_advance(char const **);

__EXTERN_C_END_DECLS

#endif
