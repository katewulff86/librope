/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <utf8proc.h>

#include "builder.h"
#include "chunk.h"
#include "concatenation.h"
#include "memory.h"
#include "misc.h"
#include "rope_private.h"

static rope    *make_small_repeating(uint8_t *, size_t, size_t);
static void     copy_multiple_times(uint8_t *, uint8_t const *, size_t, size_t);
static rope    *make_large_repeating(uint8_t *, size_t, size_t);
static rope  **make_doubling_length_ropes(uint8_t *, size_t, size_t, size_t *);
static rope   *join_long_ropes(rope **, size_t, size_t);
static rope    *make_from_generator(rope_builder *, int32_t (*)(void *),
                                    void *);

/**
 * \brief Create a new empty rope.
 * \ingroup rope_constructors
 *
 * Create an empty rope with zero length. The rope should be freed with
 * \ref rope_free when it is no longer needed.
 *
 * **TODO:**
 * Add example usage.
 *
 * \return a new empty rope
 */
rope *
rope_make_empty()
{
    return rope_make_node(NULL);
}

/**
 * \brief Create a rope of repeating characters.
 * \ingroup rope_constructors
 *
 * Create a rope of the given character repeated a given number of times.
 * Returns `NULL` if the supplied codepoint is not valid UTF-32. The rope
 * should be freed with \ref rope_free when it is no longer needed.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] length length of the resulting rope
 * \param[in] codepoint UTF-32 codepoint to be repeated
 *
 * \return a new rope containing n repetitions of the codepoint
 * \return null pointer if the codepoint is not valid
 */
rope *
rope_make_repeating(size_t length, int32_t codepoint)
{
    if (!utf8proc_codepoint_valid(codepoint))
        return NULL;
    uint8_t data[4];
    size_t bytes = utf8proc_encode_char(codepoint, data);
    if (length <= MAX_CHUNK_LENGTH)
        return make_small_repeating(data, bytes, length);
    else
        return make_large_repeating(data, bytes, length);
}

/**
 * \brief Create a small rope from a repeating codepoint
 * \ingroup internal_rope_constructors
 *
 * Create a small rope by repeating characters.
 *
 * \param[in] codepoint UTF-8 data for the codepoint
 * \param[in] codepoint_size length of the codepoint in bytes
 * \param[in] length how many times to repeat the codepoint
 * \return a new rope
 */
rope *
make_small_repeating(uint8_t *codepoint, size_t codepoint_length, size_t length)
{
    size_t size = codepoint_length * length;
    uint8_t *data = rope_allocate(size);
    copy_multiple_times(data, codepoint, codepoint_length, length);

    return rope_make_data(data, length, size);
}

/**
 * \brief Fill a buffer with repeating data
 * \ingroup internal_rope_constructors
 *
 * \param[in] to where to copy the data
 * \param[in] from data to be copied
 * \param[in] from_size how many bytes long the data is
 * \param[in] length how many times to repeat the data
 */
void
copy_multiple_times(uint8_t *to, uint8_t const *from, size_t from_length,
                    size_t times)
{
    for (size_t i = 0; i < times; ++i)
        memcpy(&to[i*from_length], from, from_length);
}

/**
 * \brief Create a rope by repeating a codepoint many times
 * \ingroup internal_rope_constructors
 *
 * Create a long rope by repeating characters. This is performed quickly by
 * reusing storage. We can create an initial rope of size S, then append that
 * rope to itself to create a rope of length 2S, then append that to itself
 * to create a rope of length 4S, and so on.
 *
 * The initial rope is of length `MAX_CHUNK_LENGTH`, and new ropes are created
 * which double in length, up to the length we need to achieve. We can then add
 * these exponentially long ropes together to create the length we require.
 *
 * \param[in] codepoint UTF-8 data for the codepoint
 * \param[in] codepoint_length length of the codepoint in bytes
 * \param[in] length how many times to repeat the codepoint
 * \return a new rope
 */
rope *
make_large_repeating(uint8_t *codepoint, size_t codepoint_length, size_t length)
{
    size_t long_ropes_length;
    rope **long_ropes = make_doubling_length_ropes(codepoint, codepoint_length,
                                                   length, &long_ropes_length);
    rope *result = join_long_ropes(long_ropes, long_ropes_length, length);
    rope_deallocate(long_ropes);

    /* If `length` was not an exact power of two multiple of MAX_CHUNK_LENGTH,
     * add the leftover. */
    size_t remaining_length = length - rope_length(result);
    if (remaining_length > 0) {
        rope *end = make_small_repeating(codepoint, codepoint_length,
                                         remaining_length);
        result = rope_append_bang(result, end);
    }

    return result;
}

rope **
make_doubling_length_ropes(uint8_t *codepoint, size_t codepoint_length,
                           size_t length, size_t *result_length)
{
    rope **ropes = rope_allocate(sizeof(*ropes) * 64);
    ropes[0] = make_small_repeating(codepoint, codepoint_length,
                                    MAX_CHUNK_LENGTH);
    size_t index;
    for (index = 1; rope_length(ropes[index-1]) * 2 <= length; ++index)
        ropes[index] = rope_append(ropes[index-1], ropes[index-1]);

    *result_length = index;

    return ropes;
}

rope *
join_long_ropes(rope **long_ropes, size_t long_ropes_length, size_t length)
{
    rope *result = rope_make_empty();

    /* Append these longer ropes together in decreasing order. */
    for (int i = long_ropes_length - 1; i >= 0; --i) {
        size_t long_rope_length = rope_length(long_ropes[i]);
        if (length >= long_rope_length) {
            length -= long_rope_length;
            result = rope_append_bang(result, long_ropes[i]);
        } else {
            rope_free(long_ropes[i]);
        }
    }

    return result;
}

/**
 * \brief Create a rope from an existing C string.
 * \ingroup rope_constructors
 *
 * Create a new rope from an existing C string. Note that this function
 * does not take ownership of the string. That is, when the rope generated
 * from it is destroyed, it will not free the C string. This means it is safe
 * to call this function on a static string, but if called on a string allocated
 * on the heap, the C string will need to be freed manually.
 *
 * The string is treated as UTF-8. Invalid code sequences are skipped. The rope
 * should be freed with \ref rope_free when it is no longer needed.
 *
 * Example usage:
 * ```c
 *     rope *hello_rope = rope_make_string("Hello, ");
 *     char *world_string = calloc(sizeof(*string), 10);
 *     strcpy(world_string, "World!");
 *     rope *world_rope = rope_make_string(world_string);
 *     free(world_string)
 *     rope *hello_world_rope = rope_append(hello_rope, world_rope);
 *     rope_free(hello_rope);
 *     rope_free(world_rope);
 *     rope_free(hello_world_rope);
 * ```
 *
 * \param[in] string a c string
 * \return a rope with the same contents as the string
 */
rope *
rope_make_string(char const *string)
{
    rope_builder *builder = rope_builder_make(true);

    while (true) {
        int32_t codepoint = extract_codepoint_and_advance(&string);
        if (codepoint == EOF)
            break;
        rope_builder_add_codepoint(builder, codepoint);
    }

    return rope_builder_extract_rope_bang(builder);
}

/**
 * \brief Create a rope from a tabulation function
 * \ingroup rope_constructors
 *
 * Create a rope by calling a function for each integer from 0 to `length`
 * (exclusive). The final rope, assuming all codepoints are valid, will be
 * `length` long. Any invalid codepoints will be skipped. The rope should be
 * freed with \ref rope_free when it is no longer needed.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] func a function which accepts an integer and returns a UTF-32
 * codepoint
 * \param[in] env an environment variable to be passed to func
 * \param[in] length
 * \return a rope
 */
rope *
rope_make_tabulate(int32_t (*func)(size_t, void *), void *env, size_t length)
{
    rope_builder *builder = rope_builder_make(true);

    for (size_t i = 0; i < length; ++i) {
        int32_t codepoint = func(i, env);
        if (!utf8proc_codepoint_valid(codepoint))
            continue;
        rope_builder_add_codepoint(builder, codepoint);
    }

    return rope_builder_extract_rope_bang(builder);
}


/**
 * \brief Create a rope from a generator function
 * \ingroup rope_constructors
 *
 * Create a rope by repeatedly calling a generator function, until the generator
 * returns `EOF`. Any invalid codepoints returned by the generator will be
 * skipped. The rope should be freed with \ref rope_free when it is no longer
 * needed.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] func a function which takes no arguments (apart from an
 * environment) and returns a codepoint
 * \param[in] env an environment variable to be passed to func
 * \return a rope
 */
rope *
rope_make_generator(int32_t (*func)(void *), void *env)
{
    rope_builder *builder = rope_builder_make(true);

    return make_from_generator(builder, func, env);
}

/**
 * \brief Create a rope from a generator function
 * \ingroup rope_constructors
 *
 * Create a rope by repeatedly calling a generator function, until the generator
 * returns `EOF`. Any invalid codepoints returned by the generator will be
 * skipped. This function is the same as \ref rope_make_generator, except that
 * the rope will be constructed from right-to-left. The rope should be freed
 * with \ref rope_free when it is no longer needed.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] func a function which takes no arguments (apart from an
 * environment) and returns a codepoint
 * \param[in] env an environment variable to be passed to func
 * \return a rope
 */
rope *
rope_make_generator_right(int32_t (*func)(void *), void *env)
{
    rope_builder *builder = rope_builder_make(false);

    return make_from_generator(builder, func, env);
}

rope *
make_from_generator(rope_builder *builder, int32_t (*func)(void *), void *env)
{
    int32_t codepoint;

    while (EOF != (codepoint = func(env))) {
        if (utf8proc_codepoint_valid(codepoint))
            rope_builder_add_codepoint(builder, codepoint);
    }

    return rope_builder_extract_rope_bang(builder);
}

/**
 * \brief Create a complete copy of a rope
 * \ingroup rope_constructors
 *
 * Create a copy of a rope. Note that because ropes share storage,
 * this only requires *O(1)* space.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope rope to be cloned
 * \return a rope
 */
rope *
rope_clone(rope const *rope)
{
    return rope_make_node(rope->root);
}

