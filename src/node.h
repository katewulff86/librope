/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_NODE_H
#define ROPE_NODE_H

#include <stdatomic.h>

#include "rope.h"

typedef struct rope_node rope_node;

struct rope_node {
    atomic_size_t rc;
    size_t length;              /* How many codepoints are in this node. */
    size_t size;                /* The size of the text data in bytes. The least
                                 * significant bit of `size' is used
                                 * as a boolean to determine if this is a leaf
                                 * or an internal tree node. */
};

__EXTERN_C_BEGIN_DECLS

bool            rope_node_leaf_p(rope_node const *);
rope_node      *rope_node_get_left(rope_node const *);
rope_node      *rope_node_get_right(rope_node const *);
size_t          rope_node_get_length(rope_node const *);
size_t          rope_node_get_size(rope_node const *);
size_t          rope_node_get_height(rope_node const *);
void            rope_node_set_length(rope_node *, size_t);
void            rope_node_set_size(rope_node *, size_t);
void            rope_node_set_leaf(rope_node *, bool);
void            rope_node_extract(rope_node const *, char *, size_t);
rope_node      *rope_node_rc_keep(rope_node const *);
void            rope_node_rc_drop(rope_node const *);

__EXTERN_C_END_DECLS

#endif
