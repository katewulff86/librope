/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_PATH_SPLITTING_H
#define ROPE_PATH_SPLITTING_H

#include "path.h"

__EXTERN_C_BEGIN_DECLS

void            rope_path_bifurcate(rope_path const *, size_t, rope_node **,
                                    rope_node **);
rope_node      *rope_path_prefix(rope_path const *, size_t);
rope_node      *rope_path_suffix(rope_path const *, size_t);

__EXTERN_C_END_DECLS

#endif

