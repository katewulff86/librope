/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include "path_movement.h"

static rope_path *successor_parent_path(rope_path const *);
static rope_path *successor(rope_path const *);
static rope_path *predecessor_parent_path(rope_path const *);
static rope_path *predecessor(rope_path const *);

rope_path *
rope_path_next(rope_path const *path)
{
    rope_path *parent = successor_parent_path(path);

    if (parent == NULL)
        return NULL;

    return successor(parent);
}

rope_path *
successor_parent_path(rope_path const *path)
{
    rope_path const *previous = path;
    rope_path *parent = path->next;

    while (parent != NULL && rope_path_came_from_right_child_p(previous)) {
        previous = parent;
        parent = parent->next;
    }

    return parent;
}

rope_path *
successor(rope_path const *path)
{
    rope_node *element = path->element;
    rope_node *child = rope_node_get_right(element);
    path = rope_path_make_parent(path, child, true);

    while (!rope_node_leaf_p(child)) {
        child = rope_node_get_left(child);
        rope_path *next = rope_path_make_parent(path, child, false);
        rope_path_rc_drop((rope_path *) path);
        path = next;
    }

    return (rope_path *) path;
}

rope_path *
rope_path_prev(rope_path const *path)
{
    rope_path *parent = predecessor_parent_path(path);

    if (parent == NULL)
        return NULL;

    return predecessor(parent);
}

rope_path *
predecessor_parent_path(rope_path const *path)
{
    rope_path const *previous = path;
    rope_path *parent = path->next;

    while (parent != NULL && rope_path_came_from_left_child_p(previous)) {
        previous = parent;
        parent = parent->next;
    }

    return parent;
}

rope_path *
predecessor(rope_path const *path)
{
    rope_node *element = path->element;
    rope_node *child = rope_node_get_left(element);
    path = rope_path_make_parent(path, child, false);

    while (!rope_node_leaf_p(child)) {
        child = rope_node_get_right(child);
        rope_path *next = rope_path_make_parent(path, child, true);
        rope_path_rc_drop((rope_path *) path);
        path = next;
    }

    return (rope_path *) path;
}

