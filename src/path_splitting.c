/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include "chunk.h"
#include "path_splitting.h"
#include "tree.h"

static rope_node *attach_right_child_to_tree(rope_node const *,
                                             rope_node const *);
static rope_node *attach_left_child_to_tree(rope_node const *,
                                            rope_node const *);

void
rope_path_bifurcate(rope_path const *path, size_t offset,
                    rope_node **left_node, rope_node **right_node)
{
    rope_chunk *chunk = (rope_chunk *) (path->element);
    rope_chunk_bifurcate(chunk, offset, (rope_chunk **) left_node,
                         (rope_chunk **) right_node);

    rope_path const *previous = path;
    path = path->next;
    while (path != NULL) {
        rope_node *element = path->element;
        if (rope_path_came_from_right_child_p(previous))
            *left_node = attach_left_child_to_tree(element, *left_node);
        else
            *right_node = attach_right_child_to_tree(element, *right_node);
        previous = path;
        path = path->next;
    }
}

rope_node *
attach_right_child_to_tree(rope_node const *element, rope_node const *node)
{
    rope_node *right_child = rope_node_get_right(element);
    if (node == NULL)
        return right_child;

    rope_node *joined = (rope_node *) rope_tree_make(node, right_child);
    rope_node_rc_drop(node);
    rope_node_rc_drop(right_child);

    return joined;
}

rope_node *
attach_left_child_to_tree(rope_node const *element, rope_node const *node)
{
    rope_node *left_child = rope_node_get_left(element);
    if (node == NULL)
        return left_child;

    rope_node *joined = (rope_node *) rope_tree_make(left_child, node);
    rope_node_rc_drop(left_child);
    rope_node_rc_drop(node);

    return joined;
}

rope_node *
rope_path_prefix(rope_path const *path, size_t offset)
{
    rope_chunk *chunk = (rope_chunk *) (path->element);
    rope_node *node = (rope_node *) rope_chunk_prefix(chunk, offset);

    rope_path const *previous = path;
    path = path->next;
    while (path != NULL) {
        rope_node const *element = path->element;
        if (rope_path_came_from_right_child_p(previous))
            node = attach_left_child_to_tree(element, node);
        previous = path;
        path = path->next;
    }

    return node;
}

rope_node *
rope_path_suffix(rope_path const *path, size_t offset)
{
    rope_chunk *chunk = (rope_chunk *) (path->element);
    rope_node *node = (rope_node *) rope_chunk_suffix(chunk, offset);

    rope_path const *previous = path;
    path = path->next;
    while (path != NULL) {
        rope_node *element = path->element;
        if (rope_path_came_from_left_child_p(previous))
            node = attach_right_child_to_tree(element, node);
        previous = path;
        path = path->next;
    }

    return node;
}

