/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "cursor_splitting.h"
#include "rope_private.h"

/**
 * \brief Replace a segment of a rope with the contents of another
 * \ingroup rope_replacement
 *
 * Replace the contents of `rope_1` from `start` (inclusive) to `end`
 * (exclusive) with the contents of `rope_2`. If `start` == `end`,
 * this splices `rope_2` into `rope_1`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope_1 rope to be replaced
 * \param[in] rope_2 contents of rope to be used in replacement
 * \param[in] start index where to start replacement (inclusive)
 * \param[in] end index where to end replacement (exclusive)
 * \return rope with replaced contents
 */
rope *
rope_replace(rope const *rope_1, rope const *rope_2, size_t start, size_t end)
{
    rope_cursor *start_cursor = rope_cursor_make(rope_1, start);
    rope_cursor *end_cursor = rope_cursor_make(rope_1, end);
    rope *prefix = rope_cursor_prefix(start_cursor);
    rope *suffix = rope_cursor_suffix(end_cursor);
    rope *joined_prefix = rope_append(prefix, rope_2);
    rope *result = rope_append(joined_prefix, suffix);
    rope_cursor_free(start_cursor); rope_cursor_free(end_cursor);
    rope_free(prefix); rope_free(suffix); rope_free(joined_prefix);

    return result;
}

