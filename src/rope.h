/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_H
#define ROPE_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifndef __EXTERN_C_BEGIN_DECLS
#ifdef __cplusplus
#define __EXTERN_C_BEGIN_DECLS extern "c" {
#define __EXTERN_C_END_DECLS }
#else
#define __EXTERN_C_BEGIN_DECLS
#define __EXTERN_C_END_DECLS
#endif
#endif

typedef struct rope rope;
typedef struct rope_cursor rope_cursor;

/**
 * \enum rope_grammar
 * \ingroup datatypes
 * \brief Grammar types for \ref rope_join and \ref rope_split.
 */
typedef enum rope_grammar {
    ROPE_GRAMMAR_INFIX,
    ROPE_GRAMMAR_SUFFIX,
    ROPE_GRAMMAR_PREFIX,
} rope_grammar;

__EXTERN_C_BEGIN_DECLS

/* Basic functions taken from
 * https://srfi.schemers.org/srfi-152/srfi-152.html and
 * https://srfi.schemers.org/srfi-130/srfi-130.html */

/**
 * \defgroup rope_constructors Rope construction functions
 */
rope           *rope_make_empty(void);
rope           *rope_make_repeating(size_t length, int32_t codepoint);
rope           *rope_make_string(char const *string);
rope           *rope_make_tabulate(int32_t (*)(size_t, void *), void *, size_t);
rope           *rope_make_generator(int32_t (*)(void *), void *);
rope           *rope_make_generator_right(int32_t (*)(void *), void *);
rope           *rope_clone(rope const *);

/**
 * \defgroup cursor_functions Basic cursor functions
 */
rope_cursor    *rope_cursor_make(rope const *, size_t);
rope_cursor    *rope_cursor_make_start(rope const *);
rope_cursor    *rope_cursor_make_end(rope const *);
rope_cursor    *rope_cursor_clone(rope_cursor const *);
rope_cursor    *rope_cursor_next(rope_cursor const *);
rope_cursor    *rope_cursor_next_bang(rope_cursor *);
rope_cursor    *rope_cursor_prev(rope_cursor const *);
rope_cursor    *rope_cursor_prev_bang(rope_cursor *);
rope_cursor    *rope_cursor_forward(rope_cursor const *, size_t);
rope_cursor    *rope_cursor_back(rope_cursor const *, size_t);
bool            rope_cursor_eq_p(rope_cursor const *, rope_cursor const *);
bool            rope_cursor_lt_p(rope_cursor const *, rope_cursor const *);
bool            rope_cursor_le_p(rope_cursor const *, rope_cursor const *);
bool            rope_cursor_gt_p(rope_cursor const *, rope_cursor const *);
bool            rope_cursor_ge_p(rope_cursor const *, rope_cursor const *);
ptrdiff_t       rope_cursor_diff(rope_cursor const *, rope_cursor const *);
size_t          rope_cursor_index(rope_cursor const *);
int32_t         rope_cursor_ref(rope_cursor const *);
bool            rope_cursor_start_p(rope_cursor const *);
bool            rope_cursor_end_p(rope_cursor const *);

/**
 * \defgroup rope_predicates Rope predicate functions
 */
bool            rope_null_p(rope const *);
bool            rope_any(rope const *, bool (*)(int32_t, void *), void *);
bool            rope_any_start_end(rope const *, bool (*)(int32_t, void *),
                                   void *, size_t, size_t);
bool            rope_any_cursor(bool (*)(int32_t, void *), void *,
                                rope_cursor const *, rope_cursor const *);
bool            rope_every(rope const *, bool (*)(int32_t, void *), void *);
bool            rope_every_start_end(rope const *, bool (*)(int32_t, void *),
                                     void *, size_t, size_t);
bool            rope_every_cursor(bool (*)(int32_t, void *), void *,
                                  rope_cursor const *, rope_cursor const *);

/**
 * \defgroup rope_selection Rope selection functions
 */
char           *rope_extract(rope const *);
char           *rope_extract_start_end(rope const *, size_t, size_t);
char           *rope_extract_cursor(rope_cursor const *, rope_cursor const *);
size_t          rope_length(rope const *);
int32_t         rope_ref(rope const *, size_t);
rope           *rope_subrope_start_end(rope const *, size_t, size_t);
rope           *rope_subrope_cursor(rope_cursor const *, rope_cursor const *);
rope           *rope_take(rope const *, size_t);
rope           *rope_drop(rope const *, size_t);
rope           *rope_take_right(rope const *, size_t);
rope           *rope_drop_right(rope const *, size_t);
rope           *rope_pad(rope const *, size_t, int32_t);
rope           *rope_pad_start_end(rope const *, size_t, int32_t, size_t,
                                   size_t);
rope           *rope_pad_cursor(size_t, int32_t, rope_cursor const *,
                                rope_cursor const *);
rope           *rope_pad_right(rope const *, size_t, int32_t);
rope           *rope_pad_right_start_end(rope const *, size_t, int32_t, size_t,
                                         size_t);
rope           *rope_pad_right_cursor(size_t, int32_t, rope_cursor const *,
                                      rope_cursor const *);
rope           *rope_trim(rope const *, bool (*)(int32_t, void *), void *);
rope           *rope_trim_start_end(rope const *, bool (*)(int32_t, void *),
                                    void *, size_t, size_t);
rope           *rope_trim_cursor(bool (*)(int32_t, void *), void *,
                                 rope_cursor const *, rope_cursor const *);
rope           *rope_trim_right(rope const *, bool (*)(int32_t, void *),
                                void *);
rope           *rope_trim_right_start_end(rope const *,
                                          bool (*)(int32_t, void *), void *,
                                          size_t, size_t);
rope           *rope_trim_right_cursor(bool (*)(int32_t, void *), void *,
                                       rope_cursor const *,
                                       rope_cursor const *);
rope           *rope_trim_both(rope const *, bool (*)(int32_t, void *),
                               void *);
rope           *rope_trim_both_start_end(rope const *,
                                         bool (*)(int32_t, void *), void *,
                                         size_t, size_t);
rope           *rope_trim_both_cursor(bool (*)(int32_t, void *), void *,
                                      rope_cursor const *, rope_cursor const *);

/**
 * \defgroup rope_replacement Rope replacement functions
 */
rope           *rope_replace(rope const *, rope const *, size_t, size_t);
rope           *rope_replace_start_end(rope const *, rope const *, size_t,
                                       size_t, size_t, size_t);
rope           *rope_replace_cursor(rope_cursor const *, rope_cursor const *,
                                    rope_cursor const *, rope_cursor const *);
rope           *rope_replace_string_start_end(rope const *, char const *,
                                              size_t, size_t);
rope           *rope_replace_string_cursor(char const *, rope_cursor const *,
                                           rope_cursor const *);

/**
 * \defgroup rope_insertion Rope insertion functions
 */
rope           *rope_insert(rope const *, rope const *, size_t);
rope           *rope_insert_cursor(rope const *, rope_cursor const *);
rope           *rope_insert_string(rope const *, char const *, size_t);
rope           *rope_insert_string_cursor(char const *, rope_cursor const *);

/**
 * \defgroup rope_comparison Rope comparison functions
 */
bool            rope_eq_p(rope const *, rope const *);
bool            rope_eq_ci_p(rope const *, rope const *);
bool            rope_lt_p(rope const *, rope const *);
bool            rope_lt_ci_p(rope const *, rope const *);
bool            rope_le_p(rope const *, rope const *);
bool            rope_le_ci_p(rope const *, rope const *);
bool            rope_gt_p(rope const *, rope const *);
bool            rope_gt_ci_p(rope const *, rope const *);
bool            rope_ge_p(rope const *, rope const *);
bool            rope_ge_ci_p(rope const *, rope const *);

/**
 * \defgroup rope_prefix_and_suffix Rope prefix and suffic functions
 */
size_t          rope_prefix_length(rope const *, rope const *);
size_t          rope_prefix_length_start_end(rope const *, rope const *, size_t,
                                             size_t, size_t, size_t);
size_t          rope_prefix_length_cursor(rope_cursor const *,
                                          rope_cursor const *,
                                          rope_cursor const *,
                                          rope_cursor const *);
size_t          rope_suffix_length(rope const *, rope const *);
size_t          rope_suffix_length_start_end(rope const *, rope const *, size_t,
                                             size_t, size_t, size_t);
size_t          rope_suffix_length_cursor(rope_cursor const *,
                                          rope_cursor const *,
                                          rope_cursor const *,
                                          rope_cursor const *);
bool            rope_prefix_p(rope const *, rope const *);
bool            rope_prefix_start_end_p(rope const *, rope const *, size_t,
                                        size_t, size_t, size_t);
bool            rope_prefix_cursor_p(rope_cursor const *, rope_cursor const *,
                                     rope_cursor const *, rope_cursor const *);
bool            rope_suffix_p(rope const *, rope const *);
bool            rope_suffix_start_end_p(rope const *, rope const *, size_t,
                                        size_t, size_t, size_t);
bool            rope_suffix_cursor_p(rope_cursor const *, rope_cursor const *,
                                     rope_cursor const *, rope_cursor const *);

/**
 * \defgroup rope_search Rope search functions
 */
rope_cursor    *rope_index(rope const *, bool (*)(int32_t, void *), void *);
rope_cursor    *rope_index_start_end(rope const *,
                                     bool (*)(int32_t, void *), void *,
                                     size_t, size_t);
rope_cursor    *rope_index_cursor(bool (*)(int32_t, void *), void *,
                                  rope_cursor const *, rope_cursor const *);
rope_cursor    *rope_index_right(rope const *, bool (*)(int32_t, void *),
                                 void *);
rope_cursor    *rope_index_right_start_end(rope const *,
                                           bool (*)(int32_t, void *), void *,
                                           size_t, size_t);
rope_cursor    *rope_index_right_cursor(bool (*)(int32_t, void *), void *,
                                        rope_cursor const *,
                                        rope_cursor const *);
rope_cursor    *rope_skip(rope const *, bool (*)(int32_t, void *), void *);
rope_cursor    *rope_skip_start_end(rope const *,
                                    bool (*)(int32_t, void *), void *,
                                    size_t, size_t);
rope_cursor    *rope_skip_cursor(bool (*)(int32_t, void *), void *,
                                 rope_cursor const *, rope_cursor const *);
rope_cursor    *rope_skip_right(rope const *, bool (*)(int32_t, void *),
                                void *);
rope_cursor    *rope_skip_right_start_end(rope const *,
                                          bool (*)(int32_t, void *), void *,
                                          size_t, size_t);
rope_cursor    *rope_skip_right_cursor(bool (*)(int32_t, void *), void *,
                                       rope_cursor const *,
                                       rope_cursor const *);
rope_cursor    *rope_contains(rope const *, rope const *);
rope_cursor    *rope_contains_start_end(rope const *, rope const *, size_t,
                                        size_t, size_t, size_t);
rope_cursor    *rope_contains_cursor(rope_cursor const *, rope_cursor const *,
                                     rope_cursor const *, rope_cursor const *);
rope_cursor    *rope_contains_right(rope const *, rope const *);
rope_cursor    *rope_contains_right_start_end(rope const *, rope const *,
                                              size_t, size_t, size_t, size_t);
rope_cursor    *rope_contains_right_cursor(rope_cursor const *,
                                           rope_cursor const *,
                                           rope_cursor const *,
                                           rope_cursor const *);
rope           *rope_take_while(rope const *, bool (*)(int32_t, void *),
                                void *);
rope           *rope_take_while_start_end(rope const *,
                                          bool (*)(int32_t, void *), void *,
                                          size_t, size_t);
rope           *rope_take_while_cursor(bool (*)(int32_t, void *), void *,
                                       rope_cursor const *,
                                       rope_cursor const *);
rope           *rope_take_while_right(rope const *, bool (*)(int32_t, void *),
                                      void *);
rope           *rope_take_while_right_start_end(rope const *,
                                                bool (*)(int32_t, void *),
                                                void *, size_t, size_t);
rope           *rope_take_while_right_cursor(bool (*)(int32_t, void *), void *,
                                             rope_cursor const *,
                                             rope_cursor const *);
rope           *rope_drop_while(rope const *, bool (*)(int32_t, void *),
                                void *);
rope           *rope_drop_while_start_end(rope const *,
                                          bool (*)(int32_t, void *), void *,
                                          size_t, size_t);
rope           *rope_drop_while_cursor(bool (*)(int32_t, void *),
                                       void *, rope_cursor const *,
                                       rope_cursor const *);
rope           *rope_drop_while_right(rope const *, bool (*)(int32_t, void *),
                                      void *);
rope           *rope_drop_while_right_start_end(rope const *,
                                                bool (*)(int32_t, void *),
                                                void *, size_t, size_t);
rope           *rope_drop_while_right_cursor(bool (*)(int32_t, void *), void *,
                                             rope_cursor const *,
                                             rope_cursor const *);
void            rope_span(rope const *, bool (*)(int32_t, void *), void *,
                          rope **, rope **);
void            rope_span_start_end(rope const *, bool (*)(int32_t, void *),
                                    void *, size_t, size_t, rope **, rope **);
void            rope_span_cursor(bool (*)(int32_t, void *),
                                 void *, rope_cursor const *,
                                 rope_cursor const *, rope **, rope **);
void            rope_break(rope const *, bool (*)(int32_t, void *), void *,
                           rope **, rope **);
void            rope_break_start_end(rope const *, bool (*)(int32_t, void *),
                                     void *, size_t, size_t, rope **, rope **);
void            rope_break_cursor(bool (*)(int32_t, void *),
                                  void *, rope_cursor const *,
                                  rope_cursor const *, rope **, rope **);

/**
 * \defgroup rope_concatenation Rope concatenation functions
 */
rope           *rope_append(rope const *, rope const *);
rope           *rope_append_string(rope const *, char const *);
rope           *rope_prepend_string(rope const *, char const *);
rope           *rope_join(rope const **, size_t, rope const *, rope_grammar);

/**
 * \defgroup rope_fold Rope fold, map and friends
 */
void           *rope_fold(void *(*)(int32_t, void *, void *), void *, void *,
                          rope const *);
void           *rope_fold_start_end(void *(*)(int32_t, void *, void *), void *,
                                    void *, rope const *, size_t, size_t);
void           *rope_fold_cursor(void *(*)(int32_t, void *, void *), void *,
                                 void *, rope_cursor const *,
                                 rope_cursor const *);
void           *rope_fold_right(void *(*)(int32_t, void *, void *), void *,
                                void *, rope const *);
void           *rope_fold_right_start_end(void *(*)(int32_t, void *, void *),
                                          void *, void *, rope const *, size_t,
                                          size_t);
void           *rope_fold_right_cursor(void *(*)(int32_t, void *, void *),
                                       void *, void *, rope_cursor const *,
                                       rope_cursor const *);
rope           *rope_map(int32_t (*)(int32_t, void *), void *, rope const *);
rope           *rope_map_many(int32_t (*)(int32_t *, size_t, void *), void *,
                              rope **, size_t);
void            rope_for_each(void (*)(int32_t, void *), void *, rope const *);
void            rope_for_each_many(void (*)(int32_t *, size_t, void *), void *,
                                   rope **, size_t);
size_t          rope_count(rope const *, bool (*)(int32_t, void *), void *);
size_t          rope_count_start_end(rope const *, bool (*)(int32_t, void *),
                                     void *, size_t, size_t);
size_t          rope_count_cursor(bool (*)(int32_t, void *), void *,
                                  rope_cursor const *, rope_cursor const *);
rope           *rope_filter(bool (*)(int32_t, void *), void *, rope const *);
rope           *rope_filter_start_end(bool (*)(int32_t, void *), void *,
                                      rope const *, size_t, size_t);
rope           *rope_filter_cursor(bool (*)(int32_t, void *), void *,
                                   rope_cursor const *, rope_cursor const *);
rope           *rope_remove(bool (*)(int32_t, void *), void *, rope const *);
rope           *rope_remove_start_end(bool (*)(int32_t, void *), void *,
                                      rope const *, size_t, size_t);
rope           *rope_remove_cursor(bool (*)(int32_t, void *), void *,
                                   rope_cursor const *, rope_cursor const *);
rope           *rope_upcase(rope const *);
rope           *rope_upcase_start_end(rope const *, size_t, size_t);
rope           *rope_upcase_cursor(rope_cursor const *, rope_cursor const *);
rope           *rope_downcase(rope const *);
rope           *rope_downcase_start_end(rope const *, size_t, size_t);
rope           *rope_downcase_cursor(rope_cursor const *, rope_cursor const *);
rope           *rope_foldcase(rope const *);
rope           *rope_foldcase_start_end(rope const *, size_t, size_t);
rope           *rope_foldcase_cursor(rope_cursor const *, rope_cursor const *);
rope           *rope_titlecase(rope const *);
rope           *rope_titlecase_start_end(rope const *, size_t, size_t);
rope           *rope_titlecase_cursor(rope_cursor const *, rope_cursor const *);

/**
 * \defgroup rope_splitting Rope replication and splitting functions
 */
rope           *rope_replicate(rope const *, size_t, size_t);
rope           *rope_replicate_start_end(rope const *, size_t, size_t, size_t,
                                         size_t);
rope           *rope_replicate_cursor(rope_cursor const *, rope_cursor const *,
                                      rope_cursor const *, rope_cursor const *);
rope          **rope_segment(rope const *, size_t, size_t *);
rope          **rope_split(rope const *, rope const *, rope_grammar, size_t,
                           size_t *);
rope          **rope_split_start_end(rope const *, rope const *, rope_grammar,
                                     size_t, size_t, size_t, size_t *);
rope          **rope_split_cursor(rope const *, rope_grammar,
                                  size_t, rope_cursor const *,
                                  rope_cursor const *, size_t *);
void            rope_bifurcate(rope const *, size_t, rope **,
                               rope **);
void            rope_bifurcate_cursor(rope_cursor const *, rope **,
                                      rope **);

/**
 * \defgroup rope_modification Rope modification functions
 */
rope           *rope_set(rope const *, size_t, int32_t);
rope           *rope_set_cursor(rope_cursor const *, int32_t);
rope           *rope_fill(rope const *, int32_t);
rope           *rope_fill_start_end(rope const *, int32_t, size_t, size_t);
rope           *rope_fill_cursor(int32_t, rope_cursor const *,
                                 rope_cursor const *);
rope           *rope_copy(rope const *, size_t, rope const *);
rope           *rope_copy_start_end(rope const *, size_t, rope const *, size_t,
                                    size_t);
rope           *rope_copy_cursor(rope_cursor const *, rope_cursor const *,
                                 rope_cursor const *);
rope           *rope_reverse(rope const *);
rope           *rope_reverse_start_end(rope const *, size_t, size_t);
rope           *rope_reverse_cursor(rope_cursor const *, rope_cursor const *);

void            rope_free(rope *);
void            rope_cursor_free(rope_cursor *);
size_t          rope_get_object_count();

__EXTERN_C_END_DECLS

#endif
