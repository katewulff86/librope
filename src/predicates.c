/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>

#include "cursor.h"
#include "rope_private.h"

/**
 * \brief Check if a rope is empty.
 * \ingroup rope_predicates
 *
 * Return `true` if the given rope is the empty rope, `false` otherwise.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope the rope to check
 * \return `true` for the empty rope, `false` otherwise
 */
bool
rope_null_p(rope const *rope)
{
    return rope->root == NULL;
}

/**
 * \brief Check if a predicate is true for any codepoint in a rope.
 * \ingroup rope_predicates
 *
 * Return `true` if the given predicate is true for any codepoint in a rope,
 * and `false` otherwise. This function is short-circuiting, and will stop
 * as soon as the predicate returns true for any codepoint. Returns `false`
 * when using any predicate on the empty rope.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope the rope to check
 * \param[in] predicate the predicate used to check
 * \param[in] env environment passed to the predicate
 * \return `true` if the predicate is true for any codepoint in the rope
 */
bool
rope_any(rope const *rope, bool (*predicate)(int32_t, void *), void *env)
{
    return rope_any_start_end(rope, predicate, env, 0, rope_length(rope));
}

/**
 * \brief Check if a predicate is true for any codepoint in a range of a rope.
 * \ingroup rope_predicates
 *
 * Return `true` if the given predicate is true for any codepoint within a
 * range of the rope from start (inclusive) to end (exclusive),
 * and `false` otherwise. This function is short-circuiting, and will stop
 * as soon as the predicate returns true for any codepoint. Returns `false`
 * if passed an empty range. The return value is unspecified if `end` < `start`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope the rope to check
 * \param[in] predicate the predicate used to check
 * \param[in] env environment passed to the predicate
 * \param[in] start index of the start of the range (inclusive)
 * \param[in] end index of the end of the range (exclusive)
 * \return `true` if the predicate is true for any codepoint in the range of
 * the rope
 */
bool
rope_any_start_end(rope const *rope, bool (*predicate)(int32_t, void *),
                   void *env, size_t start, size_t end)
{
    rope_cursor *start_cursor = rope_cursor_make(rope, start);
    rope_cursor *end_cursor = rope_cursor_make(rope, end);
    bool result = rope_any_cursor(predicate, env, start_cursor, end_cursor);
    rope_cursor_free(start_cursor);
    rope_cursor_free(end_cursor);

    return result;
}

/**
 * \brief Check if a predicate is true for any codepoint in a range of a rope.
 * \ingroup rope_predicates
 *
 * Return `true` if the given predicate is true for any codepoint within a
 * range of the rope from start (inclusive) to end (exclusive),
 * and `false` otherwise. This function is short-circuiting, and will stop
 * as soon as the predicate returns true for any codepoint. Returns `false`
 * for any predicate if passed an empty range. The return value is unspecified
 * if the cursors point to different ropes, or if `end` < `start`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] predicate the predicate used to check
 * \param[in] env environment passed to the predicate
 * \param[in] start cursor of the start of the range (inclusive)
 * \param[in] end cursor of the end of the range (exclusive)
 * \return `true` if the predicate is true for any codepoint in the range of
 * the rope, `false` otherwise
 */
bool
rope_any_cursor(bool (*predicate)(int32_t, void *), void *env,
                rope_cursor const *start, rope_cursor const *end)
{
    if (!rope_cursor_le_p(start, end))
        return false;

    bool result = false;
    rope_cursor *cursor;
    for (cursor = rope_cursor_clone(start);
         rope_cursor_lt_p(cursor, end);
         cursor = rope_cursor_next_bang(cursor)) {
        int32_t codepoint = rope_cursor_ref(cursor);
        if (predicate(codepoint, env)) {
            result = true;
            break;
        }
     }

    rope_cursor_free(cursor);

    return result;
}

/**
 * \brief Check if a predicate is true for every codepoint in a rope.
 * \ingroup rope_predicates
 *
 * Return `true` if the given predicate is true for every codepoint in a rope,
 * and `false` otherwise. This function is short-circuiting, and will stop
 * as soon as the predicate returns false for any codepoint. Returns `true`
 * for any predicate if passed an empty rope.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope the rope to check
 * \param[in] predicate the predicate used to check
 * \param[in] env environment passed to the predicate
 * \return `true` if the predicate is true for every codepoint in the rope
 */
bool
rope_every(rope const *rope, bool (*predicate)(int32_t, void *), void *env)
{
    return rope_every_start_end(rope, predicate, env, 0, rope_length(rope));
}

/**
 * \brief Check if a predicate is true for every codepoint in a range of a rope.
 * \ingroup rope_predicates
 *
 * Return `true` if the given predicate is true for every codepoint within a
 * range of the rope from start (inclusive) to end (exclusive),
 * and `false` otherwise. This function is short-circuiting, and will stop
 * as soon as the predicate returns false for any codepoint. Returns `true`
 * for any predicate if passed an empty range. The return value is unspecified
 * if `end` < `start`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] rope the rope to check
 * \param[in] predicate the predicate used to check
 * \param[in] env environment passed to the predicate
 * \param[in] start index of the start of the range (inclusive)
 * \param[in] end index of the end of the range (exclusive)
 * \return `true` if the predicate is true for every codepoint in the range of
 * the rope
 */
bool
rope_every_start_end(rope const *rope, bool (*predicate)(int32_t, void *),
                   void *env, size_t start, size_t end)
{
    rope_cursor *start_cursor = rope_cursor_make(rope, start);
    rope_cursor *end_cursor = rope_cursor_make(rope, end);
    bool result = rope_every_cursor(predicate, env, start_cursor, end_cursor);
    rope_cursor_free(start_cursor);
    rope_cursor_free(end_cursor);

    return result;
}

/**
 * \brief Check if a predicate is true for every codepoint in a range of a rope.
 * \ingroup rope_predicates
 *
 * Return `true` if the given predicate is true for every codepoint within a
 * range of the rope from start (inclusive) to end (exclusive),
 * and `false` otherwise. This function is short-circuiting, and will stop
 * as soon as the predicate returns false for any codepoint. Returns `true`
 * if passed an empty range.  The return value is unspecified if the cursors
 * point to different ropes, or if `end` < `start`.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] predicate the predicate used to check
 * \param[in] env environment passed to the predicate
 * \param[in] start cursor of the start of the range (inclusive)
 * \param[in] end cursor of the end of the range (exclusive)
 * \return `true` if the predicate is true for every codepoint in the range of
 * the rope, `false` otherwise
 * \return `true` if the two cursors do not belong to the same rope
 */
bool
rope_every_cursor(bool (*predicate)(int32_t, void *), void *env,
                rope_cursor const *start, rope_cursor const *end)
{
    if (!rope_cursor_le_p(start, end))
        return true;

    bool result = true;
    rope_cursor *cursor;
    for (cursor = rope_cursor_clone(start);
         rope_cursor_lt_p(cursor, end);
         cursor = rope_cursor_next_bang(cursor)) {
        int32_t codepoint = rope_cursor_ref(cursor);
        if (!predicate(codepoint, env)) {
            result = false;
            break;
        }
     }

    rope_cursor_free(cursor);

    return result;
}

