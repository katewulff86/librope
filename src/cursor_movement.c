/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include "cursor_construction.h"
#include "cursor_movement.h"
#include "path_movement.h"

static rope_cursor *cursor_to_next_chunk_bang(rope_cursor *);
static rope_cursor *cursor_to_next_codepoint_bang(rope_cursor *);
static rope_cursor *cursor_to_prev_chunk_bang(rope_cursor *);
static rope_cursor *cursor_to_prev_codepoint_bang(rope_cursor *);

/**
 * \brief Move a cursor to the next codepoint
 * \ingroup cursor_functions
 *
 * Move a cursor to the next codepoint in the rope to which the cursor points.
 * If the cursor is already past the end, no change is made. Note that this
 * functions always returns a new cursor, it does not modify the cursor passed
 * into it.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] cursor a cursor into a rope
 * \return a cursor which points to the next codepoint
 */
rope_cursor *
rope_cursor_next(rope_cursor const *cursor)
{
    rope_cursor *clone = rope_cursor_clone(cursor);

    return rope_cursor_next_bang(clone);
}

/**
 * \brief Mutate a cursor to point to the next codepoint
 * \ingroup cursor_functions
 *
 * Mutate a cursor to point to the next codepoint. This procedure may or may not
 * modify the cursor in place, so the returned cursor must be used. This
 * procedure is very useful in a `for` loop.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] cursor a cursor into a rope
 * \return a cursor which points to the next codepoint
 */
rope_cursor *
rope_cursor_next_bang(rope_cursor *cursor)
{
    if (rope_cursor_end_p(cursor))
        return cursor;

    cursor->index += 1;
    if (rope_path_end_p(cursor->path, cursor->offset))
        return cursor_to_next_chunk_bang(cursor);
    else
        return cursor_to_next_codepoint_bang(cursor);
}

rope_cursor *
cursor_to_next_chunk_bang(rope_cursor *cursor)
{
    rope_path *path = rope_path_next(cursor->path);
    rope_path_rc_drop(cursor->path);
    cursor->path = path;
    cursor->offset = 0;

    return cursor;
}

rope_cursor *
cursor_to_next_codepoint_bang(rope_cursor *cursor)
{
    cursor->offset = rope_path_next_offset(cursor->path, cursor->offset);

    return cursor;
}
/**
 * \brief Move a cursor to the previous codepoint
 * \ingroup cursor_functions
 *
 * Move a cursor to the previous codepoint in the rope to which the cursor
 * points. If the cursor is already at the start, no change is made. Note that
 * this functions always returns a new cursor, it does not modify the cursor
 * passed into it.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] cursor a cursor into a rope
 * \return a cursor which points to the next codepoint
 */
rope_cursor *
rope_cursor_prev(rope_cursor const *cursor)
{
    rope_cursor *clone = rope_cursor_clone(cursor);

    return rope_cursor_prev_bang(clone);
}

/**
 * \brief Mutate a cursor to point to the previous codepoint
 * \ingroup cursor_functions
 *
 * Mutate a cursor to point to the previous codepoint. Note that this procedure
 * may or may not modify the cursor in place, so the returned cursor must be
 * used. This procedure is very useful in a `for` loop.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] cursor a cursor into a rope
 * \return a cursor which points to the previous codepoint
 */
rope_cursor *
rope_cursor_prev_bang(rope_cursor *cursor)
{
    if (rope_cursor_start_p(cursor))
        return cursor;
    if (rope_cursor_end_p(cursor)) {
        rope_node *root = cursor->root;
        rope_cursor_free(cursor);
        return rope_cursor_make_from_root(root, rope_node_get_length(root) - 1);
    }

    cursor->index = cursor->index - 1;
    if (cursor->offset > 0) {
        return cursor_to_prev_codepoint_bang(cursor);
    } else {
        return cursor_to_prev_chunk_bang(cursor);
    }
}

rope_cursor *
cursor_to_prev_codepoint_bang(rope_cursor *cursor)
{
    cursor->offset = rope_path_prev_offset(cursor->path, cursor->offset);

    return cursor;
}

rope_cursor *
cursor_to_prev_chunk_bang(rope_cursor *cursor)
{
    rope_path *path = rope_path_prev(cursor->path);
    rope_path_rc_drop(cursor->path);
    cursor->path = path;
    cursor->offset = rope_path_offset(cursor->path, cursor->index);

    return cursor;
}

/**
 * \brief Move a cursor forward
 * \ingroup cursor_functions
 *
 * Move a cursor `n` codepoints forward in a rope. If this would cause the
 * cursor to point beyond the end of the rope, the cursor returned will point
 * to one past the end, as if returned by \ref rope_cursor_make_end.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] cursor a cursor into a rope
 * \param[in] n how many codepoints to move
 * \return a cursor which points `n` codepoints forward
 */
rope_cursor *
rope_cursor_forward(rope_cursor const *cursor, size_t n)
{
    if (n == 0)
        return rope_cursor_clone(cursor);
    return rope_cursor_make_from_root(cursor->root, cursor->index + n);
}

/**
 * \brief Move a cursor backward
 * \ingroup cursor_functions
 *
 * Move a cursor `n` codepoints backward in a rope. If this would cause the
 * cursor to point before the start of the rope, the cursor returned will point
 * to the start, as if returned by \ref rope_cursor_make_start.
 *
 * **TODO:**
 * Add example usage.
 *
 * \param[in] cursor a cursor into a rope
 * \param[in] n how many codepoints to move
 * \return a cursor which points `n` codepoints backward
 */
rope_cursor *
rope_cursor_back(rope_cursor const *cursor, size_t n)
{
    if (n == 0)
        return rope_cursor_clone(cursor);
    if (n >= cursor->index)
        return rope_cursor_make_from_root(cursor->root, 0);
    return rope_cursor_make_from_root(cursor->root, cursor->index - n);
}

