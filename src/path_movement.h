/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_PATH_MOVEMENT_H
#define ROPE_PATH_MOVEMENT_H

#include "path.h"

__EXTERN_C_BEGIN_DECLS

rope_path      *rope_path_next(rope_path const *);
rope_path      *rope_path_prev(rope_path const *);

__EXTERN_C_END_DECLS

#endif

