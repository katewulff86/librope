/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>

#include "cursor_splitting.h"
void
rope_bifurcate(rope const *r, size_t index, rope **left, rope **right)
{
    rope_cursor *cursor = rope_cursor_make(r, index);
    if (rope_cursor_end_p(cursor)) {
        *left = rope_clone(r);
        *right = rope_make_empty();
    } else {
        rope_bifurcate_cursor(cursor, left, right);
    }
    rope_cursor_free(cursor);
}

void
rope_bifurcate_cursor(rope_cursor const *cursor, rope **left, rope **right)
{
    rope_cursor_bifurcate(cursor, left, right);
}

