/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_CURSOR_SPLITTING_H
#define ROPE_CURSOR_SPLITTING_H

#include "cursor.h"

__EXTERN_C_BEGIN_DECLS

void            rope_cursor_bifurcate(rope_cursor const *, rope **, rope **);
rope           *rope_cursor_prefix(rope_cursor const *);
rope           *rope_cursor_suffix(rope_cursor const *);

__EXTERN_C_END_DECLS

#endif

