/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_CURSOR_CONSTRUCTION_H
#define ROPE_CURSOR_CONSTRUCTION_H

#include "cursor.h"

__EXTERN_C_BEGIN_DECLS

rope_cursor    *rope_cursor_make_eof(rope const *);
rope_cursor    *rope_cursor_make_from_root(rope_node *, size_t);
rope_cursor    *rope_cursor_make_eof_from_root(rope_node *);

__EXTERN_C_END_DECLS

#endif

