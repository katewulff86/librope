/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include "chunk.h"
#include "memory.h"
#include "node.h"
#include "tree.h"

inline bool
rope_node_leaf_p(rope_node const *root)
{
    return root->size % 2;
}

inline rope_node *
rope_node_get_left(rope_node const *node)
{
    if (rope_node_leaf_p(node))
        return NULL;

    return rope_tree_get_left((rope_tree *) node);
}

inline rope_node *
rope_node_get_right(rope_node const *node)
{
    if (rope_node_leaf_p(node))
        return NULL;

    return rope_tree_get_right((rope_tree *) node);
}

inline size_t
rope_node_get_length(rope_node const *node)
{
    if (node == NULL)
        return 0;

    return node->length;
}

inline size_t
rope_node_get_size(rope_node const *node)
{
    if (node == NULL)
        return 0;

    return node->size >> 1;
}

inline size_t
rope_node_get_height(rope_node const *node)
{
    if (node == NULL || rope_node_leaf_p(node))
        return 0;

    return rope_tree_get_height((rope_tree *) node);
}

inline void
rope_node_set_length(rope_node *node, size_t length)
{
    node->length = length;
}

inline void
rope_node_set_size(rope_node *node, size_t size)
{
    bool leaf = rope_node_leaf_p(node);
    node->size = (size << 1) + leaf;
}

inline void
rope_node_set_leaf(rope_node *node, bool leaf)
{
    if (leaf)
        node->size |= 0x1;
    else
        node->size &= ~0x1;
}

void
rope_node_extract(rope_node const *node, char *string, size_t offset)
{
    if (node == NULL)
        return;

    if (rope_node_leaf_p(node))
        rope_chunk_extract((rope_chunk *) node, string, offset);
    else
        rope_tree_extract((rope_tree *) node, string, offset);
}

inline rope_node *
rope_node_rc_keep(rope_node const *node)
{
    if (node != NULL)
        ((rope_node *) node)->rc++;

    return (rope_node *) node;
}

void
rope_node_rc_drop(rope_node const *node)
{
    if (node == NULL)
        return;

    if (node->rc == 1) {
        if (rope_node_leaf_p(node))
            rope_chunk_free((rope_chunk *) node);
        else
            rope_tree_free((rope_tree *) node);
        rope_deallocate((void *) node);
    } else {
        ((rope_node *) node)->rc -= 1;
    }
}

