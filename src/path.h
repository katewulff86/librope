/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_PATH_H
#define ROPE_PATH_H

#include "node.h"

typedef struct rope_path rope_path;

/* Trees do not store parent points, as this would create cycles and complicate
 * reference counting, and would also mean that subtrees could not be reused
 * (import for persistent data structures). A cursor needs to be able to find
 * its way through a tree, so it stores its path to the current node. */
struct rope_path {
    atomic_size_t rc;
    rope_node *element;
    rope_path *next;
    size_t index;
    bool right_child;
};

__EXTERN_C_BEGIN_DECLS

rope_path      *rope_path_make(rope_node const *, size_t);
rope_path      *rope_path_make_parent(rope_path const *, rope_node const *,
                                      bool);
rope_node      *rope_path_root(rope_path const *);
size_t          rope_path_offset(rope_path const *, size_t);
size_t          rope_path_next_offset(rope_path const *, size_t);
size_t          rope_path_prev_offset(rope_path const *, size_t);
bool            rope_path_end_p(rope_path const *, size_t);
int32_t         rope_path_ref(rope_path const *, size_t);
bool            rope_path_came_from_left_child_p(rope_path const *);
bool            rope_path_came_from_right_child_p(rope_path const *);

rope_path      *rope_path_rc_keep(rope_path *);
void            rope_path_rc_drop(rope_path *);

__EXTERN_C_END_DECLS

#endif

