/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef ROPE_CURSOR_PREDICATES_H
#define ROPE_CURSOR_PREDICATES_H

#include "cursor.h"

__EXTERN_C_BEGIN_DECLS

bool            rope_cursor_same_rope(rope_cursor const *, rope_cursor const *);

__EXTERN_C_END_DECLS

#endif

